# Container

* 最常用的默认布局！只能包含一个child:，支持配置 padding,margin,color,宽高,decoration（一般配置边框和阴影）等配置，
* 在 Flutter 中，不是所有的控件都有 宽高、padding、margin、color 等属性，所以有 Padding、Center 等 Widget。



设置 黑色遮罩的效果

![黑色遮罩的效果](https://images.gitee.com/uploads/images/2019/0623/175239_c00d6b49_568055.png "屏幕截图.png")

```java
var testContainer = new Container(
    // color 与 decoration 不可同时设置
    //color: Colors.white,
    //四周10大小的maring
    margin: EdgeInsets.all(10.0),
    height: 120.0,
    width: 500.0,
    //透明黑色遮罩
    decoration: new BoxDecoration(
        ///弧度为4.0
        borderRadius: BorderRadius.all(Radius.circular(4.0)),
        //设置了decoration的color，就不能设置Container的color。
        color: Colors.black,
        ///边框
        border: new Border.all(color: Colors.red, width: 0.3)),
    child: new Text("666666"));


```
设置背景颜色的效果

```java
var testColorContainer = new Container(
  // color 与 decoration 不可同时设置
  color: Colors.white,
  //外间距
  margin: EdgeInsets.all(10.0),
  //内间距
  padding: EdgeInsets.all(10.0),
  height: 120.0,
  width: 500.0,
  child: new Text("666666"),
);

```

不设置padding 的效果

![输入图片说明](https://images.gitee.com/uploads/images/2019/0623/175302_c515b914_568055.png "屏幕截图.png")

设置padding 的效果

![输入图片说明](https://images.gitee.com/uploads/images/2019/0623/175330_827cbf61_568055.png "屏幕截图.png")



