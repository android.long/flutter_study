Flutter Stack 帧布局，层叠堆放


两个Image堆放在一起

```java

new Stack(
              children: <Widget>[
                new Image(
                  image: imageUrl,
                  width: 300.0,
                  height: 200.0,
                  fit: BoxFit.cover,
                ),
                //控制位置
                new Positioned(
                  right: 15.0,
                  top: 15.0,
                  child: new Icon(
                    Icons.share,
                    color: Colors.white,
                  ),
                ),
              ],
            )
```

