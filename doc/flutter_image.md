# Image 简介

Android ios 原生中使用 ImageView 来加载显示图片
在flutter 中通过Image来加载并显示图片






### 1 Image 加载网络图片

```java

var imageUrl = "http://pic31.nipic.com/20130711/8952533_164845225000_2.jpg";

Image(
  image: NetworkImage(
      imageUrl),
  width: 100.0,
)

或者

Image.network(
  imageUrl,
  scale: 8.5,
),

或者

new FadeInImage.assetNetwork(
                    //占位图
                    placeholder: 'images/logo.png',
                    image: imageUrl,
                    width: 120,
                    fit: BoxFit.fitWidth,
                  ),

或者

 new CachedNetworkImage(
              width: double.infinity,
              height: double.infinity,
              fit: BoxFit.fill,
              imageUrl: imageUrl,
              placeholder: (context, url) => new ProgressView(),
              errorWidget: (context, url, error) => new Icon(Icons.error),
            ),
```

![输入图片说明](https://images.gitee.com/uploads/images/2019/0624/224027_113357c5_568055.png "屏幕截图.png")


### 2 Image 加载本地图片

```java

new Image.file(
  File('/storage/emulated/0/Download/test.jpg'),
  width: 120,
  fit: BoxFit.fill,
  //fill(全图显示且填充满，图片可能会拉伸)，contain（全图显示但不充满，显示原比例），cover（显示可能拉伸，也可能裁剪，充满）
  //fitWidth（显示可能拉伸，可能裁剪，宽度充满），fitHeight显示可能拉伸，可能裁剪，高度充满），scaleDown（效果和contain差不多，但是）
),


```

### 3 Image 加载assets图片

#### 3.1 在工程根目录下创建一个images目录，并将图片xxx.png拷贝到该目录。
#### 3.2 在pubspec.yaml中的flutter部分添加如下内容：

```
  assets:
    - images/xxx.png
```
#### 3.3 代码中使用
```java
Image(
  image: AssetImage("images/xxx.png"),
  width: 100.0
);
或
Image.asset("images/xxx.png",
  width: 100.0,
)

```


### 4 加载圆角图片 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0624/224502_5560d557_568055.png "屏幕截图.png")

```
new ClipOval(
                  child: Image.network(
                    imageUrl,
                    width: 100,
                    height: 100,
                    fit: BoxFit.fitHeight,
                  ),
                ),

或者
new CircleAvatar(
                  backgroundImage: NetworkImage(imageUrl),
                  radius: 50.0,
                ),

```

### 4 加载圆角矩形图片

![输入图片说明](https://images.gitee.com/uploads/images/2019/0624/224517_a16ba4ee_568055.png "屏幕截图.png")

```
 new Container(
                  width: 120,
                  height: 60,
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(5),
                      bottomLeft: Radius.circular(5),
                    ),
                    image: DecorationImage(
                        image: NetworkImage(imageUrl),
//                        image: AssetImage('images/icon_main_bg.png'),
                        fit: BoxFit.cover),
                  ),
                ),

或者


new ClipRRect(
                  child: Image.network(
                    imageUrl,
                    scale: 8.5,
                    fit: BoxFit.cover,
                  ),
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20),
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  ),
                ),

```


### 3 Image 属性说明　

```
const Image({
  ...
  this.width, //图片的宽
  this.height, //图片高度
  this.color, //图片的混合色值
  this.colorBlendMode, //混合模式
  this.fit,//缩放模式
  this.alignment = Alignment.center, //对齐方式
  this.repeat = ImageRepeat.noRepeat, //重复方式
  ...
})

```
#### 3.1 width、height

width、height：用于设置图片的宽、高，当不指定宽高时，图片会根据当前父容器的限制，尽可能的显示其原始大小，如果只设置width、height的其中一个，那么另一个属性默认会按比例缩放，但可以通过下面介绍的fit属性来指定适应规则。

#### 3.2 fit

fit：该属性用于在图片的显示空间和图片本身大小不同时指定图片的适应模式。适应模式是在BoxFit中定义，它是一个枚举类型，有如下值：

fill：会拉伸填充满显示空间，图片本身长宽比会发生变化，图片会变形。
cover：会按图片的长宽比放大后居中填满显示空间，图片不会变形，超出显示空间部分会被剪裁。
contain：这是图片的默认适应规则，图片会在保证图片本身长宽比不变的情况下缩放以适应当前显示空间，图片不会变形。
fitWidth：图片的宽度会缩放到显示空间的宽度，高度会按比例缩放，然后居中显示，图片不会变形，超出显示空间部分会被剪裁。
fitHeight：图片的高度会缩放到显示空间的高度，宽度会按比例缩放，然后居中显示，图片不会变形，超出显示空间部分会被剪裁。
none：图片没有适应策略，会在显示空间内显示图片，如果图片比显示空间大，则显示空间只会显示图片中间部分。

