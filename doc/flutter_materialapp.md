#### Flutter MaterialApp


##### 1 作用 

    MaterialApp 代表 Material 设计风格的应用。常用于定义APP 的统一默认风格，用作程序的入口
    
##### 2 案例一 
    
    ```
    import 'package:flutter/material.dart';

    import 'package:flutter/material.dart';


    //Flutter程序入口
    void main() => runApp(getApp());
    
    Widget getApp() {
      return new MaterialApp(
        //应用默认所显示的界面 页面
        home: new Container(color: Colors.grey,),
        //应用的顶级导航表格，多页面应用 控制页面跳转
        //routes: ,
        // 当系统修改语言的时候，会触发å这个回调
        //onLocaleChanged ：
        // 应用各种 UI 所使用的主题颜色
        theme: ThemeData.light().copyWith(
          primaryColor: Colors.grey[850],
          accentColor: Colors.grey[850],
          indicatorColor: Colors.white,
        ),
      );
    }


    ```

![输入图片说明](https://images.gitee.com/uploads/images/2019/0622/213033_7fbbc2e3_568055.png "屏幕截图.png")