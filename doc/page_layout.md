### 布局


  在安卓中,使用到的布局 线性布局,相对布局,帧布局,绝对布局.
  在flutter中,有 线性布局Row和Column,弹性布局,流式布局,层叠布局


#### 1 线性布局Row和Column

Flutter中通过Row和Column来实现线性布局，类似于Android中的LinearLayout控件。Row和Column都继承自Flex.

 * 对于线性布局，有主轴和纵轴之分，如果布局是沿水平方向，那么主轴就是指水平方向，而纵轴即垂直方向；
 * 如果布局沿垂直方向，那么主轴就是指垂直方向，而纵轴就是水平方向。
 * 在线性布局中，有两个定义对齐方式的枚举类MainAxisAlignment和CrossAxisAlignment，分别代表主轴对齐和纵轴对齐。

在这里只构造了 Column的使用,Row 原理一至,只不过是方向不一样
```java
Widget buildCoulm(){
    // 构造 Android Java 语言中 LinearLayout orientation="vertical"  中的垂直布局
    //Expanded 在 Column 和 Row 中代表着平均充满，当有两个以上的widget存在的时候默认均分充满。同时页可以设置 flex 属性决定比例
    return Column(
      //表示水平方向子widget的布局顺序(是从左往右还是从右往左)，默认为系统当前Locale环境的文本方向(如中文、英语都是从左往右，而阿拉伯语是从右往左)
      textDirection:TextDirection.ltr,
      //即是竖直向居中（官方描述为 主轴居中）
      /**
       * 表示子Widgets在Column所占用的竖直空间内对齐方式，
       * 如果mainAxisSize值为MainAxisSize.min，则此属性无意义，因为子widgets的高度等于Row的高度。
       * 只有当mainAxisSize的值为MainAxisSize.max时，此属性才有意义，
       *    MainAxisAlignment.start表示沿textDirection的初始方向对齐，
       *    如textDirection取值为TextDirection.ltr时，则MainAxisAlignment.start表示左对齐，textDirection取值为TextDirection.rtl时表示从右对齐。
       *    而MainAxisAlignment.end和MainAxisAlignment.start正好相反；MainAxisAlignment.center表示居中对齐。
       *
       */
      mainAxisAlignment: MainAxisAlignment.center,
      //默认是最大充满、还是根据child显示最小大小 这里设置为大小按照最小显示
      //表示Column在主轴(竖直)方向占用的空间，默认是MainAxisSize.max，表示尽可能多的占用竖直方向的空间，
      // 此时无论子widgets实际占用多少水平空间，Column的宽度始终等于竖直方向的最大高度；
      //而MainAxisSize.min表示尽可能少的占用竖直空间，当子widgets没有占满水平剩余空间，则Row的实际宽度等于所有子widgets占用的的竖直空间
      mainAxisSize: MainAxisSize.min,

      //表示Row纵轴（垂直）的对齐方向，默认是VerticalDirection.down，表示从上到下
      verticalDirection: VerticalDirection.down,

      //水平方向 左对齐
      crossAxisAlignment: CrossAxisAlignment.start,
      // 添加三个 Text 垂直显示
      children: <Widget>[
        new Text("test1 "),
        new Text("test2 "),
        new Text("test3 "),
      ],
    );

  }

```



#### 2 弹性布局

弹性布局允许子widget按照一定比例来分配父容器空间，
弹性布局的概念在其UI系统中也都存在，如H5中的弹性盒子布局，Android中的FlexboxLayout。
Flutter中的弹性布局主要通过Flex和Expanded来配合实现。

Flex可以沿着水平或垂直方向排列子widget，Row和Column都继承自Flex.

```java
//构造 Android Java 语言中 LinearLayout orientation="horizontal"  中的垂直布局
 Widget buildFlexLayout() {
    return Column(
      children: <Widget>[
        //Flex的两个子widget按1：2来占据水平空间
        Flex(
          //子Widget的排列方式
          direction: Axis.horizontal,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                height: 30.0,
                color: Colors.red,
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                height: 30.0,
                color: Colors.blue,
              ),
            ),
          ],
        ),
      ],
    );
  }
```


#### 3 流式布局

Flutter把超出屏幕显示范围会自动折行的布局称为流式布局。
Flutter中通过Wrap和Flow来支持流式布局。

Wrap的很多属性在Row（包括Flex和Column）中也有,可以认为Wrap和Flex（包括Row和Column）除了超出显示范围后Wrap会折行外，其它行为基本相同。


#### 4 层叠布局
 Flutter中使用Stack和Positioned来实现绝对定位。
 Stack允许子widget堆叠，而Positioned可以给子widget定位（根据Stack的四个角）。
