
### StatefulWidget和StatelessWidget

StatefulWidget和StatelessWidget是Flutter中所有Widget的两个分类。

StatefulWidget 在需要更新UI的时候调用setState(VoidCallback fn)，并在回调函数中改变一些变量数值等，
组件会重新build以达到刷新状态也就是刷新UI的效果。

而 StatelessWidget 并不具有 StatefulWidget 的功能，所以我们一般定义页面的时候，都会继承于 StatefulWidget而实现的。
因为一般情况下我们的页面UI 会根据数据的改变而改变 ，所以我们前面的页面写法是无法再次更新 UI 显示效果

对于前页面的案例中

```java

import 'package:flutter/material.dart';

//Flutter程序入口
void main() => runApp(getApp());

Widget getApp() {
  return new MaterialApp(
    //应用默认所显示的界面 页面
    home: getAllScaffold(),
  );
}

... ...


```

我们可以改造成 

```
import 'package:flutter/material.dart';
import 'IndexPage.dart';

//Flutter程序入口
void main() => runApp(getApp());

Widget getApp() {
  return new MaterialApp(
    //应用默认所显示的界面 页面
    home: IndexPage(),
  );
}

```

在这里创建了一个单独的文件 IndexPage.dart,在这里的效果与前者是一至的,不同的是,后者可以数据的不同来更新UI ，后面的文章会逐渐提到

```
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class IndexPage extends StatefulWidget {
  @override
  IndexPagePageState createState() => IndexPagePageState();
}

class IndexPagePageState extends State<IndexPage> {
  @override
  Widget build(BuildContext context) {
    return buildPage();
  }

  //构造页面主体
  Widget buildPage() {
    return new Scaffold(
      //显示在界面顶部的一个 AppBar
      appBar: buildDefaultBar("test"),
      //body：当前界面所显示的主要内容
      body: new Container(color: Colors.grey),
    );
  }

  //构造 AppBar
  Widget buildDefaultBar(String title) {
    return AppBar(
      //标题居中显示
      centerTitle: true,
      //返回按钮占位
      leading: Container(),
      //标题显示
      title: Text(title),
    );
  }
}

```



