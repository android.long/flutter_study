#### Dart语言 基础--

main()函数 不论在Dart还是Flutter中，必须都需要一个顶层的main()函数，它是整个应用的入口函数，main()函数的返回值是void，还有一个可选的参数，参数类型是List<String>。

```
main() {

}
```
#### 1 基本数据类型
##### 1.2 Dart有如下几种内建的数据类型
在 Dart 里是强类型的，也有自动推断类型的机制.

类型 | 说明
------- | -------
Number | 可以理解为是 数字数据类型
String | 字符串
Boolean | 布尔类型
List | 数组
Map | 键值数据类型
##### 1.3 Number
包括两类，int 整形，double 浮点形
##### 1.4 Boolean
 取值只有 true false

##### 1.5 List 类似java中的数组与集合

```java
var list =[1,2,4,5];
```
##### 1.6 Map 类似java中的hashmap

##### 1.7 字符串 strings

```java
// 1.Dart可以使用单引号或双引号来创建字符串
var s1 = "hello";
var s2 = 'world';

// 2.类似Python，Dart可以使用三引号来创建包含多行的字符串 这里使用三个双引号来 当然也可以使用三个单引号
var multiLine1 = """多行的字符串内容
多行的字符串内容
多行的字符串内容
多行的字符串内容
""";

var multiLine2 = '''你也可以使用三个单引号，创建一个
包含了多行的字符串内容
''';

// 3.类似Python，还可以在字符串字面值的前面加上`r`来创建原始字符串，则该字符串中特殊字符可以不用转义
var path = r'D:\workspace\code';

// 4.Dart支持使用"+"操作符拼接字符串
var greet = "hello" + " world";

// 5.Dart提供了插值表达式"${}"，也可以用于拼接字符串
var name = "王五";
var aStr = "hello,${name}";
print(aStr);    // hello,王五

// 当仅取变量值时，可以省略花括号
var aStr2 = "hello,$name"; // hello,王五

// 当拼接的是一个表达式时，则不能省略花括号
var str1 = "link";
var str2 = "click ${str1.toUpperCase()}";
print(str2);   // click LINK

// 6. 与Java不同，Dart使用"=="来比较字符串的内容
print("hello" == "world");
---------------------
```
##### 1.4 map 为关联数组，相当于Java中的HashMap
```java
// 1.通过字面量创建Map
var gifts = {
  'first' : 'partridge',
  'second': 'turtledoves',
  'fifth' : 'golden rings'
};

// 2.使用Map类的构造函数创建对象
var pic = new Map();
// 往Map中添加键值对
pic['first'] = 'partridge';
pic['second'] = 'turtledoves';
pic['fifth'] = 'golden rings';

// 3.获取Map的长度
print(pic.length);

// 4.查找Map
pirnt(pic["first"]);
print(pic["four"]);    // 键不存在则返回 null

```
##### 2 基本变量的声明
Dart中定义变量有两种方式，一种是静态类型语言常用的方式，显式指定变量类型，另一种则是动态语言的常用方式，不指定类型，由vm自动推断。

```java
// 1.通过显式指定类型来定义变量
String name = "张三";
num age = 18;

// 2.使用关键字var，不指定类型
var address = "深南大道";
var id = 100;

/* 使用var定义变量，即使未显式指定类型，一旦赋值后类型就被固定
 * 因此使用var定义的变量不能改变数据类型
 */
var number = 19;
// 以下代码错误，无法运行，number变量已确定为int类型
number = "2019";


```
如想动态改变变量的数据类型，应当使用dynamic或Object来定义变量。
dynamic 是任意的意思，它与 var 不同，var 会自动推断类型从而得出一个确定类型，而 dynamic 可以表示任意，相对于 Typescript 中的 any。
```java
// dynamic声明变量
//在这里初始化的是一个sting 类型的
dynamic var1 = "hello";
//在这里又改变成了 int 类型
var1 = 19;
print(var1);    // 19

// Object声明变量
Object var2 = 20;
var2 = "Alice";
print(var2);    // Alice

```

Dart中定义常量也有两种方式，一种使用final关键字，同Java中的用法， 一个 final 变量只能赋值一次；另一种是Dart的方式，使用const关键字定义。

```java
// 1.使用final关键字定义常量
final height = 10;

// 2.使用const关键字定义常量
const pi = 3.14;
//需要注意，final定义的常量是运行时常量，而const常量则是编译时常量，也就是说final定义常量时，其值可以是一个变量，而const定义的常量，其值必须是一个字面常量值。
```








##### 3 Dart中的函数使用
从java 或者 oc 的开发者过来，声明函数时有 返回值，就必须要有return，我们可理解为非常严谨的编程语言，但是在Dart语言中，我们可以理解为非严谨的编程函数
```
// 声明返回值
int add(int a, int b) {
  return a + b;
}
// 不声明返回值
add2(int a, int b) {
  return a + b;
}

// =>是return语句的简写
add3(a, b) => a + b;

```

在主函数中使用时 直接调用就可以了 var add2 = add(1, 2);


##### 3.1 命名参数
顾名思义 就是给参数定了个名字如下

```

//一个参数
test({String name}){
  print("hello, my name is $name and age is 0");
}
//多个参数
test({String name,int age}){
  print("hello, my name is $name and age is $age");
}
```
与一般的函数声明的区别是 在声明参数类型处多了一个{}}
并且在调用时为 add4(name: "zhansan",age: 3);
也就是在输入参数值的前面必须加上 参数的对应的名称
```
// 命名参数的默认值
// 不能写成：int add({a: int, b: int = 3})
int add({int a, int b = 3}) {
  return a + b;
}

```


##### 3.2 位置参数
使用中括号[]括起来的参数是函数的位置参数，代表该参数可传可不传，位置参数只能放在函数的参数列表的最后面

```
// 位置参数可以有多个，比如[String a, int b]
test(String name, int age, [String hobby]) {
  StringBuffer sb = new StringBuffer();
  sb.write("hello, this is $name and I am $age years old");
  if (hobby != null) {
    sb.write(", my hobby is $hobby");
  }
  print(sb.toString());
}


// 命名参数的默认值
// 不能写成：int add({a: int, b: int = 3})
int add({int a, int b = 3}) {
  return a + b;
}

// 位置参数的默认值
int sum(int a, int b, [int c = 3]) {
  return a + b + c;
}


```

##### 3.3函数作为参数

```
main() {
  //这里传入了一个匿名函数
  test((param) {
    // 打印hello
    print(param);
  });
}


test(Function callback) {
  callback("hello");
}

```


##### 4 运算符

```java
+、-、*、/、%同Java语言
```
Dart中又多出了一个整除运算符~/，与普通除号的区别是将相除后的结果取整返回。

类型 | 说明
------- | -------
as | 用于类型转换
is | 如果对象是指定的类型就返回 True
is! | 如果对象不是指定的类型返回 True

当 obj 实现了 T 的接口时， obj is T 才是 true。类似于Java中的instanceof。
Dart中使用 as 操作符把对象转换为特定的类型，如无法转换则会抛出异常，因此在转换前最好使用is运算符进行检测。

```java
// 将p转换为Person类型再操作
(p as Person).name = 'Bruce';
```


##### 5 控制流程
if / else switch for /while try / catch语句跟Java中都类似，try / catch语句可能稍有不同


```
 // if else语句
  int score = 80;
  if (score < 60) {
    print("so bad!");
  } else if (score >= 60 && score < 80) {
    print("just so so!");
  } else if (score >= 80) {
    print("good job!");
  }

  // switch语句
  String a = "hello";
  // case语句中的数据类型必须是跟switch中的类型一致
  switch (a) {
    case "hello":
      print("haha");
      break;
    case "world":
      print("heihei");
      break;
    default:
      print("WTF");
  }

  // for语句
  List<String> list = ["a", "b", "c"];
  for (int i = 0; i < list.length; i++) {
    print(list[i]);
  }
  for (var i in list) {
    print(i);
  }
  // 这里的箭头函数参数必须用圆括号扩起来
  list.forEach((item) => print(item));

  // while语句
  int start = 1;
  int sum = 0;
  while (start <= 100) {
    sum += start;
    start++;
  }
  print(sum);

  // try catch语句
  try {
    print(1 ~/ 0);
  } catch (e) {
    // IntegerDivisionByZeroException
    print(e);
  }
  try {
    1 ~/ 0;
  } on IntegerDivisionByZeroException { // 捕获指定类型的异常
    print("error"); // 打印出error
  } finally {
    print("over"); // 打印出over
  }

```

##### 6 dart中的类class
##### 6.1 类的定义与构造方法
Dart中的类没有访问控制，所以你不需要用private, protected, public等修饰成员变量或成员函数，一个简单的类

```
class Person {
  String name;
  int age;
  String gender;
  //无参数构造
  Person();
  //构造方法
  Person(this.name, this.age, this.gender);
  //命名构造
  Person.builder(){};
  //成员方法
  test() {
    print("hello, this is $name, I am $age years old, I am a $gender");
  }
}

```
这里的构造方法传入的3个参数都是this.xxx，而且没有大括号{}包裹的方法体
这种语法是Dart比较独特而简洁的构造方法声明方式，它等同于下面的代码：
```
Person(String name, int age, String gender) {
    this.name = name;
    this.age = age;
    this.gender = gender;
}

```

main()函数中测试

```
//可以在构造中直接赋值
var p = new Person("zhangsan", 20, "male");
// hello, this is zhangsan, I am 20 years old, I am a male
p.test();

//也可以单独调用更新赋值
p.age = 22;
p.gender = "female";
p.test();
 // hello, this is zhangsan, I am 22 years old, I am a female

```


###### 6.2 Dart中使用extends关键字做类的继承
###### 6.3 抽象类和抽象方法
使用abstract修饰一个类，则这个类是抽象类，抽象类中可以有抽象方法和非抽象方法，抽象方法没有方法体，需要子类去实现

```
abstract class Person {
  // 抽象方法，没有方法体，需要子类去实现
  void doSomething();
  // 普通的方法
  void greet() {
    print("hello world!");
  }
}

class TestPerson extends Person {
  // 实现了父类的抽象方法
  void doSomething() {
    print("I'm doing something...");
  }
}

```


###### 7 Dart库（Libraries）
Dart目前已经有很多的库提供给开发者，许多功能不需要开发者自己去实现，只需要导入对应的包即可，使用import语句来导入某个包,或者是导入某个具体的类

```
//基本导入方式
import 'package:lib1/lib1.dart';
//可以使用as关键字为导入的某个包设置一个前缀，或者说别名
import 'package:lib2/lib2.dart' as lib2;
// 只导入foo
import 'package:lib1/lib1.dart' show foo;
// 导入除了foo的所有其他部分
import 'package:lib2/lib2.dart' hide foo;
//导入包时使用deferred as可以让这个包懒加载，懒加载的包只会在该包被使用时得到加载，而不是一开始就加载
import 'package:greetings/hello.dart' deferred as hello;



```

##### 8 Dart 异步
Dart提供了类似ES7中的async await等异步操作，这种异步操作在Flutter开发中会经常遇到，比如网络或其他IO操作，文件选择等都需要用到异步的知识。
async和await往往是成对出现的，如果一个方法中有耗时的操作，你需要将这个方法设置成async，并给其中的耗时操作加上await关键字，如果这个方法有返回值，你需要将返回值塞到Future中并返回

```
Future checkVersion() async {
  var version = await lookUpVersion();
  // Do something with version
}

```
模拟一段网络请求
```
Future<String> getNetData() async{
  http.Response res = await http.get("http://www.baidu.com");
  return res.body;
}

```










