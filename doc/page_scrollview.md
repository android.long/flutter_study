# SingleChildScrollView


ScrollView 是一个抽象类

SingleChildScrollView类似于开发中常用的ScrollView


### SingleChildScrollView

```
    Widget buildTabScaffold() {
      String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
      //Scrollbar是一个Material风格的滚动指示器（滚动条），如果要给可滚动widget添加滚动条，只需将Scrollbar作为可滚动widget的父widget即可
      //CupertinoScrollbar是iOS风格的滚动条，如果你使用的是Scrollbar，那么在iOS平台它会自动切换为CupertinoScrollbar
      return Scrollbar(
        child: SingleChildScrollView(
          // 设置滚动的方向, 默认垂直方向
          scrollDirection: Axis.vertical,
          // 设置显示方式 reverse: false，垂直方向,则滚动内容头部和左侧对其, 那么滑动方向就是从左向右
          reverse: false,
          padding: EdgeInsets.all(0.0),
          physics: BouncingScrollPhysics(),
          child: Center(
            child: Column(
              //动态创建一个List<Widget>
              children: str.split("")
              //每一个字母都用一个Text显示,字体为原来的两倍
                  .map((c) => Text(c, textScaleFactor: 2.0))
                  .toList(),
            ),
          ),
        ),
      );
    }

```


