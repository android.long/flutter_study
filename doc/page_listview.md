# ListView

这里通过 ListView.builder 来构建 ListView


### ListView

```
    Widget listViewLayoutBuilder(List<BaseBean> list) {
      return ListView.builder(
          //设置滑动方向 Axis.horizontal 水平  默认 Axis.vertical 垂直
          scrollDirection: Axis.vertical,
          //内间距
          padding: EdgeInsets.all(10.0),
          //是否倒序显示 默认正序 false  倒序true
          reverse: false,
          //false，如果内容不足，则用户无法滚动 而如果[primary]为true，它们总是可以尝试滚动。
          primary: true,
          //确定每一个item的高度 会让item加载更加高效
          itemExtent: 50.0,
          //item 高度会适配 item填充的内容的高度 多用于嵌套listView中 内容大小不确定 比如 垂直布局中 先后放入文字 listView （需要Expend包裹否则无法显示无穷大高度 但是需要确定listview高度 shrinkWrap使用内容适配不会） 文字
          shrinkWrap: true,
          //item 数量
          itemCount: list.length,
          //滑动类型设置
          //new AlwaysScrollableScrollPhysics() 总是可以滑动 NeverScrollableScrollPhysics禁止滚动 BouncingScrollPhysics 内容超过一屏 上拉有回弹效果 ClampingScrollPhysics 包裹内容 不会有回弹
    //        cacheExtent: 30.0,  //cacheExtent  设置预加载的区域   cacheExtent 强制设置为了 0.0，从而关闭了“预加载”
          physics: new ClampingScrollPhysics(),
          //滑动监听
    //        controller ,
          itemBuilder: (context, i) => new Container(
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    new Text(
                      "${list[i].name}",
                      style: new TextStyle(fontSize: 18.0, color: Colors.red),
                    ),
                    new Text(
                      "${list[i].age}",
                      style: new TextStyle(fontSize: 18.0, color: Colors.green),
                    ),
                    new Text(
                      "${list[i].content}",
                      style: new TextStyle(fontSize: 18.0, color: Colors.blue),
                    ),
                  ],
                ),
              ));
    }

```


