# AppBar

AppBar 显示在app的顶部，或者说 顶端栏，对应着 Android 的 Toolbar。如下图：

![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/154710_a6c6d470_568055.png "屏幕截图.png")

*** 

###　１ 只有标题 无其他按钮
```
  Widget buildDefaultBar(String title) {
   return appBar =  AppBar(
      //标题居中显示
      centerTitle: true,
      //返回按钮占位
      leading: Container(),
      //标题显示
      title: Text(title),
    );
  }
```

###　２ 显示标题和返回按钮　

![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/154922_eb7fd8a3_568055.png "屏幕截图.png")

```
  /**
   * title appBar 显示的标题文字
   * backIcon  appBar 显示的返回键图标
   */
  Widget buildBackBar(String title,{backIcon=Icons.arrow_back_ios}) {
    return appBar =AppBar(
      centerTitle: true,
      //在标题前面显示的一个控件，在首页通常显示应用的 logo；在其他界面通常显示为返回按钮
      leading: IconButton(
          icon: Icon(backIcon),
          onPressed: () {
            Navigator.pop(context);
          }),
      //Toolbar 中主要内容，通常显示为当前界面的标题文字
      title: Text(title),
    );
  }
```

###　３　显示标题和返回按钮和右侧的分享按钮

####　代码块封装

```
  /**
   * title appBar 显示的标题文字
   * backIcon  appBar 显示的返回键图标
   * actions  appBar 最右侧的图标集合
   */
  Widget buildBackAndOtherBar(String title,{backIcon=Icons.arrow_back_ios,List<Widget> actions}) {
    return appBar =AppBar(
      centerTitle: true,
      //在标题前面显示的一个控件，在首页通常显示应用的 logo；在其他界面通常显示为返回按钮
      leading: IconButton(
          icon: Icon(backIcon),
          onPressed: () {
            Navigator.pop(context);
          }),
      //Toolbar 中主要内容，通常显示为当前界面的标题文字
      title: Text(title),
      //标题右侧显示的按钮组
      actions:actions,
    );
  }
```

####　显示标题和返回按钮和右侧的分享按钮


```
 buildBackAndOtherBar("测试3", actions: <Widget>[
                IconButton(icon: Icon(Icons.share), onPressed: () {}),
              ]);
```


![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/155123_9189f4c5_568055.png "屏幕截图.png")


####　显示标题和返回按钮和右侧的分享按钮＋弹出框

![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/155425_8d84e2df_568055.png "屏幕截图.png")

![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/155445_ebb28c8f_568055.png "屏幕截图.png")



```
buildBackAndOtherBar("测试2", actions: <Widget>[
                IconButton(icon: Icon(Icons.share), onPressed: () {}),
                PopupMenuButton(
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuItem<String>>[
                    PopupMenuItem<String>(
                      child: Text("热度"),
                      value: "hot",
                    ),
                    PopupMenuItem<String>(
                      child: Text("最新"),
                      value: "new",
                    ),
                  ],
                  onSelected: (String action) {
                    switch (action) {
                      case "hot":
                        print("hot");
                        break;
                      case "new":
                        print("new");
                        break;
                    }
                  },
                  onCanceled: () {
                    print("onCanceled");
                  },
                )
              ]);
```


