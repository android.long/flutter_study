### tabbar



```java
Widget buildDefaultScaffold(){
    return Scaffold(
        appBar: AppBar(
          title: Text('标题'),
          //设置标题居中
          centerTitle: true,
          //设置最左侧按钮
          leading: Builder(builder: (context) {
            return IconButton(
              icon: Icon(Icons.bug_report, color: Colors.white),
              onPressed: () {
                // 打开抽屉菜单
                Scaffold.of(context).openDrawer();
              },
            );
          }),
          actions: <Widget>[ //导航栏右侧菜单
            IconButton(icon: Icon(Icons.search), onPressed: () {}),
          ],
        ),
        backgroundColor: Colors.white,
        //页面显示主体
        body: Container()
    );
  }

```


```java
  //通过“bottom”属性来添加一个导航栏底部tab按钮组，将要实现的效果如下：
  Widget buildTabScaffold(){
    List tabs = ["首页", "发现", "我的","设置"];
    //用于控制/监听Tab菜单切换
    //TabBar和TabBarView正是通过同一个controller来实现菜单切换和滑动状态同步的。
    TabController  tabController = TabController(length: tabs.length, vsync: this);
    return Scaffold(
        appBar: AppBar(
          title: Text('标题'),
          bottom: TabBar(   //生成Tab菜单
              controller: tabController,
              tabs: tabs.map((e) => Tab(text: e)).toList()
          ),
          //设置标题居中
          centerTitle: true,
          //设置最左侧按钮
          leading: Builder(builder: (context) {
            return IconButton(
              icon: Icon(Icons.bug_report, color: Colors.white),
              onPressed: () {
                // 打开抽屉菜单
                Scaffold.of(context).openDrawer();
              },
            );
          }),
          actions: <Widget>[ //导航栏右侧菜单
            IconButton(icon: Icon(Icons.search), onPressed: () {}),
          ],
        ),
        backgroundColor: Colors.white,

      body: TabBarView(
        controller: tabController,
        //创建Tab页
        children: tabs.map((e) {
          return Container(
            alignment: Alignment.center,
            child: Text(e, textScaleFactor: 1),
          );
        }).toList(),
      ),
    );
  }
```

