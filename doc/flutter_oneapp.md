#### Flutter 使用Android Studio 创建第一个应用

#### 1 创建应用

![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/171329_b721ea80_568055.png "屏幕截图.png")

#### 2 选择 Flutter application 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/172257_794765de_568055.png "屏幕截图.png")

####　３　配置　应用的相关信息

![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/172446_860e7cc7_568055.png "屏幕截图.png")

下一步　

![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/172558_313d73f5_568055.png "屏幕截图.png")

####　４

![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/172850_0872b921_568055.png "屏幕截图.png")

####　５　运行ＡＰＰ　

我这里使用的是　ｘ６测试手机

![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/173026_065858d3_568055.png "屏幕截图.png")