# 自定义提示框 Dialog


#### 1 无按钮的消息提示框
```java
  void showDefaultDialog(BuildContext context, String message) {
    if (message != null && message.isNotEmpty) {
      showDialog<Null>(
          context: context, //BuildContext对象
          barrierDismissible: false,
          builder: (BuildContext context) {
            return new MessageDialog(title: "提示", message: message);
            //调用对话框);
          });
    }
  }

```

#### 2 只有确认的消息提示框

```java
  void showToastDialog(BuildContext context, String message, {String title = "提示", String negativeText = "确定", Function callBack}) {
    if (message != null && message.isNotEmpty) {
      showDialog<Null>(
          context: context, //BuildContext对象
          barrierDismissible: false,
          builder: (BuildContext context) {
            return new MessageDialog(
              title: title,
              negativeText: negativeText,
              message: message,
              onCloseEvent: () {
                Navigator.pop(context);
                if (callBack != null) {
                  callBack();
                }
              },
            );
            //调用对话框);
          });
    }
  }

```

#### 3 有确认取消的消息提示框

```java
 void showCanDialog(BuildContext context, String message, {String title = "提示", String negativeText = "确定", String positiveText = "取消", Function callBack, Function canCallBack}) {
    if (message != null && message.isNotEmpty) {
      showDialog<Null>(
          context: context, //BuildContext对象
          barrierDismissible: false,
          builder: (BuildContext context) {
            return new MessageDialog(
              title: title,
              negativeText: negativeText,
              positiveText: positiveText,
              message: message,
              onPositivePressEvent: () {
                Navigator.pop(context);
                if (canCallBack != null) {
                  canCallBack();
                }
              },
              onCloseEvent: () {
                Navigator.pop(context);
                if (callBack != null) {
                  callBack();
                }
              },
            );
            //调用对话框);
          });
    }
  }

```

### 4 自定义一个 MessageDialog

```java
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class MessageDialog extends Dialog {
  //标题
  String title;
  //消息
  String message;
  //确认按钮
  String negativeText;
  //取消按钮
  String positiveText;
  // 确认按钮点击回调
  Function onCloseEvent;
  //取消按钮 点击回调
  Function onPositivePressEvent;

  MessageDialog({
    Key key,
    @required this.title,
    @required this.message,
    this.negativeText,
    this.positiveText,
    this.onPositivePressEvent,
    @required this.onCloseEvent,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: const EdgeInsets.all(25.0),
      child: new Material(
        type: MaterialType.transparency,
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            new Container(
              decoration: ShapeDecoration(
                color: Color(0xffffffff),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(
                    Radius.circular(8.0),
                  ),
                ),
              ),
              child: new Column(
                children: <Widget>[
                  new Padding(
                    padding: const EdgeInsets.all(10),
                    child: new Stack(
                      alignment: AlignmentDirectional.centerEnd,
                      children: <Widget>[
                        new Center(
                          child: new Text(
                            title,
                            style: new TextStyle(
                              fontSize: 19.0,
                            ),
                          ),
                        ),
                        new GestureDetector(
                          onTap: this.onCloseEvent,
                          child: new Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: new Icon(
                              Icons.close,
                              color: Color(0xffe0e0e0),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  new Container(
                    color: Color(0xffe0e0e0),
                    height: 1.0,
                  ),
                  new Container(
                    padding: EdgeInsets.fromLTRB(25, 15, 25, 15),
                    constraints: BoxConstraints(minHeight: 100.0),
                    child: Center(
                      child: new Text(
                        message,
                        style: TextStyle(fontSize: 16.0),
                      ),
                    ),
                  ),
                  this._buildBottomButtonGroup(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget _buildBottomButtonGroup() {
    var widgets = <Widget>[];
    if (negativeText != null && negativeText.isNotEmpty)
      widgets.add(_buildBottomCancelButton());
    if (positiveText != null && positiveText.isNotEmpty)
      widgets.add(_buildBottomPositiveButton());
    return Container(
      child: new Flex(
        direction: Axis.horizontal,
        children: widgets,
      ),
    );
  }

  Widget _buildBottomCancelButton() {
    return new Flexible(
      fit: FlexFit.tight,
      child: new FlatButton(
        materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
        onPressed: onCloseEvent,
        child: Padding(
          padding: EdgeInsets.fromLTRB(0, 11, 0, 11),
          child: Text(
            negativeText,
            style: TextStyle(
              fontSize: 16.0,
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildBottomPositiveButton() {
    return new Flexible(
      fit: FlexFit.tight,
      child: new FlatButton(
        onPressed: onPositivePressEvent,
        child: Padding(
          padding: EdgeInsets.fromLTRB(0, 11, 0, 11),
          child: Text(
            positiveText,
            style: TextStyle(
              color: Color(Colors.teal.value),
              fontSize: 16.0,
            ),
          ),
        ),
      ),
    );
  }
}

```



