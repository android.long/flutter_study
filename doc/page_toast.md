# Fluttertoast Toast 提示框


### Fluttertoast

```
  #导包
  #https://github.com/PonnamKarthik/FlutterToast
  fluttertoast: ^3.1.0
```
####　默认样式的Toast
```java
 Fluttertoast.showToast(msg: "show msg");
```


###  自定义样式

```
      Fluttertoast.showToast(
          msg: "This is Center Short Toast",
          //显示时间 与android 中的一样
          toastLength: Toast.LENGTH_SHORT,
          //显示位置
          gravity: ToastGravity.CENTER,
          timeInSecForIos: 1,
          //显示背景颜色
          backgroundColor: Colors.red,
          //显示文字颜色
          textColor: Colors.white,
          //字体大小
          fontSize: 16.0
      );
```


