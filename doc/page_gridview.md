# GridView 九宫格


### Default GridView

```
      Widget gridViewDefaultCount(List<BaseBean> list) {
        return GridView.count(
    //      padding: EdgeInsets.all(5.0),
          //一行多少个
          crossAxisCount: 5,
          //滚动方向
          scrollDirection: Axis.vertical,
          // 左右间隔
          crossAxisSpacing: 10.0,
          // 上下间隔
          mainAxisSpacing: 10.0,
          //宽高比
          childAspectRatio: 2 / 5,
          //设置itemView 
          children: initListWidget(list),
        );
      }
    
      List<Widget> initListWidget(List<BaseBean> list) {
        List<Widget> lists = [];
        for (var item in list) {
          lists.add(new Container(
            height: 50.0,
            width: 50.0,
            color: Colors.yellow,
            child: new Center(
                child: new Text(
              item.age.toString(),
            )),
          ));
        }
        return lists;
      }
    }

```


