# TextField 简介


### TextField 文本输入框


最简单的使用 
```java
//这会创建一个基础TextField 默认带一个下划线
TextField() 
```

最全使用

```java
   Widget buildTextField() {
     // 文本控制器
     final controller = TextEditingController();
     controller.addListener(() {
       print('input ${controller.text}');
     });
 
     //controller.text 使用这个方法来获取输入框的文字
 
     /**
      *  调用此方法 使用TextField 获取焦点
      *  FocusScope.of(context).requestFocus(nodeTwo);
      */
     FocusNode nodeOne = FocusNode();
     return Theme(
       data: new ThemeData(
           primaryColor: Colors.blue.shade300, hintColor: Colors.blue),
       child: TextField(
         controller: controller,
         //是否隐藏密码
         obscureText: false,
         //绑定焦点控制
         focusNode: nodeOne,
         /**
          * TextCapitalization.sentences  这是最常见的大写化类型，每个句子的第一个字母被转换成大写。
          * TextCapitalization.characters  大写句子中的所有字符。
          * TextCapitalization.words 对每个单词首字母大写。
          */
         textCapitalization: TextCapitalization.sentences,
         //控制光标样式
         cursorColor: Colors.red,
         cursorRadius: Radius.circular(6.0),
         cursorWidth: 6.0,
         //自动获取焦点
         autofocus: false,
         //最大长度，设置此项会让TextField右下角有一个输入数量的统计字符串
         maxLength: 30,
         //输入文本的样式
         style: TextStyle(fontSize: 14.0, color: Colors.red),
         //在弹出键盘的时候修改键盘类型
         keyboardType: TextInputType.number,
         //键盘回车键的样式
         textInputAction: TextInputAction.send,
         //允许的输入格式 WhitelistingTextInputFormatter.digitsOnly 只允许输入数字
         inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
         //内容改变的回调
         onChanged: (text) {
           print('change $text');
         },
         //内容提交(按回车)的回调
         onSubmitted: (text) {
           print('submit $text');
         },
         //键盘上按了done
         onEditingComplete: () {
 
         },
         decoration: InputDecoration(
             hintText: "请输入文字",
             hintStyle:
                 TextStyle(fontWeight: FontWeight.w300, color: Colors.red),
             //填充颜色
             fillColor: Colors.blue.shade50,
             filled: true,
             // 边框的内边距
             contentPadding: EdgeInsets.all(10.0),
             //边框
             border: OutlineInputBorder(
               //边框圆角
               borderRadius: BorderRadius.circular(5.0),
             )),
       ),
     );
   }

```


