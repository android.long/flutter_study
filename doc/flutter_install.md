#### Flutter 从配制开发环境再到开发第一个应用

从java开发走起，开发java,先下载jdk sdk,然后配制环境变量，最后配制开发工具，进入开发。
在Flutter这里，也是先下载 Flutter sdk,Dart sdk,然后配制环境变量 ，最后配制开发工具，进入开发。

#### 1 配制开发环境
##### 1.1.1 下载Flutter sdk

[官网下载(https://flutter.dev/docs/development/tools/sdk/releases?tab=windows)

不同的电脑系统下载不同的SDK 
![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/164422_d5c17c4b_568055.png "屏幕截图.png")

下载下来的 Flutter Sdk 目录 
![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/165047_f62766f7_568055.png "屏幕截图.png")

##### 1.1.1 Dark sdk

Flutter Sdk 下 包含了 Dart sdk
![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/165147_16f9b354_568055.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/165813_172884eb_568055.png "屏幕截图.png")
##### 1.2 Windows 环境变量配制

* 配制Flutter Sdk 环境变量
![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/165812_fb72dbc5_568055.png "屏幕截图.png")


再配制两个系统变量

```
FLUTTER_STORAGE_BASE_URL
https://storage.flutter-io.cn
```
![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/170040_c1f93adf_568055.png "屏幕截图.png")


```
PUB_HOSTED_URL
https://pub.flutter-io.cn
```
![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/170127_f591a750_568055.png "屏幕截图.png")


##### 1.3 MacOs 环境变量配制

打开命令行工具 [终端]


```
cd $HOME
open -e .bash_profile
```
文件中添加 
```
export PUB_HOSTED_URL=https://pub.flutter-io.cn
export FLUTTER_STORAGE_BASE_URL=https://storage.flutter-io.cn
```
添加flutter sdk 路径到path中，也就是添加 Flutter SDK 安装的路径
```
export PATH=${PATH}:/Users/.../Flutter/flutter/bin:$PATH
```




#####１.4 Android Studio Flutter 插件配制
![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/170511_a2f9d11c_568055.png "屏幕截图.png")

安装 flutter 插件
![](https://images.gitee.com/uploads/images/2019/0620/170556_abcd4e52_568055.png "屏幕截图.png")

安装 Dart 插件
![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/170625_80d107ae_568055.png "屏幕截图.png")

重启 Android Studio 
