#### Flutter  开发应用第一个页面

![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/173830_fa025ccf_568055.png "屏幕截图.png")

点击运行后　［手机上一个灰色的空页面］

![输入图片说明](https://images.gitee.com/uploads/images/2019/0620/173856_d0cf75e4_568055.png "屏幕截图.png")

#### 说明 


   在Android 开发页面,我们一般会通过 一个Activity 或者是 Fagment 来显示出一个页面,而这些页面的根本是 还是 View 或者 ViewGroup.
   在IOS 开发中,页面相关也全部是View 了.

   在Flutter 开发中, 通过 Widget 来实现上述的功能页面

    而在刚刚的代码中,Container 就是一个Widget的子类,


```
import 'package:flutter/material.dart';
//Flutter程序入口
void main() => runApp(getApp());

Widget getApp() {
  //创建一个空的页面
  return new Container(color: Colors.grey);
}
```

