# Radio 单选框



单选框记录的值 
```java 
  String _newValue = '语文';
```

#### 1 一组单选框

![输入图片说明](https://images.gitee.com/uploads/images/2019/0623/184555_75d6fb6e_568055.png "屏幕截图.png")

```java

  Widget buildRowRadio() {
    return Row(
      children: <Widget>[
        Radio<String>(
            //当前Radio的值
            value: "语文",
            //当前Radio 所在组的值
            //只有value 与groupValue 值一至时才会被选中
            groupValue: _newValue,
            onChanged: (value) {
              setState(() {
                _newValue = value;
              });
            }),
        Radio<String>(
            value: "数学",
            groupValue: _newValue,
            onChanged: (value) {
              setState(() {
                _newValue = value;
              });
            }),
        Radio<String>(
            value: "英语",
            groupValue: _newValue,
            onChanged: (value) {
              setState(() {
                _newValue = value;
              });
            }),
      ],
    );
  }

```

#### 2 横排一组显示文字的单选框 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0623/184611_714d09a9_568055.png "屏幕截图.png")

```java
 Widget buildRowFixTitleRadio() {
    return Row(
      children: <Widget>[
        Flexible(
          child: RadioListTile<String>(
            value: '语文',
            title: Text('语文'),
            groupValue: _newValue,
            onChanged: (value) {
              setState(() {
                _newValue = value;
              });
            },
          ),
        ),
        Flexible(
          child: RadioListTile<String>(
            value: '数学',
            title: Text('数学'),
            groupValue: _newValue,
            onChanged: (value) {
              setState(() {
                _newValue = value;
              });
            },
          ),
        ),
        Flexible(
          child: RadioListTile<String>(
            value: '英语',
            title: Text('英语'),
            groupValue: _newValue,
            onChanged: (value) {
              setState(() {
                _newValue = value;
              });
            },
          ),
        ),
      ],
    );
  }
```

#### 3 竖排一组显示文字的单选框 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0623/184622_6255c02c_568055.png "屏幕截图.png")

```java
 Widget buildRadio() {
    return Column(
      children: <Widget>[
        RadioListTile<String>(
          value: '语文',
          title: Text('语文'),
          groupValue: _newValue,
          onChanged: (value) {
            setState(() {
              _newValue = value;
            });
          },
        ),
        RadioListTile<String>(
          value: '数学',
          title: Text('数学'),
          groupValue: _newValue,
          onChanged: (value) {
            setState(() {
              _newValue = value;
            });
          },
        ),
        RadioListTile<String>(
          value: '英语',
          title: Text('英语'),
          groupValue: _newValue,
          onChanged: (value) {
            setState(() {
              _newValue = value;
            });
          },
        ),
      ],
    );
  }
```

