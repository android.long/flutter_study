# Switch and Checkbox

```
      activeColor → Color - 激活时原点的颜色。
      activeThumbImage → ImageProvider - 原点还支持图片，激活时的效果。
      activeTrackColor → Color - 激活时横条的颜色。
      inactiveThumbColor → Color - 非激活时原点的颜色。
      inactiveThumbImage → ImageProvider - 非激活原点的图片效果。
      inactiveTrackColor → Color - 非激活时横条的颜色。
      onChanged → ValueChanged - 改变时触发。
      value → bool - 切换按钮的值。
```


```
    bool _switchSelected = false; //维护单选开关状态
    bool _checkboxSelected = false; //维护复选框状态
```

### Switch

![输入图片说明](https://images.gitee.com/uploads/images/2019/0623/182540_20a73b83_568055.png "屏幕截图.png")

```java
    Switch(
          //当前状态
          value: _switchSelected,
          // 激活时原点颜色
          activeColor: Colors.blue,
          inactiveTrackColor: Colors.blue.shade50,
          onChanged: (value) {
            //重新构建页面
            setState(() {
              _switchSelected = value;
            });
          },
        )

```

### IOS 风格的 CupertinoSwitch 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0623/182554_b2e70c17_568055.png "屏幕截图.png")

```java
        CupertinoSwitch(
          value: _switchSelected,
          onChanged: (value) {},
        ),
```

### SwitchListTile

![输入图片说明](https://images.gitee.com/uploads/images/2019/0623/182610_ede025e7_568055.png "屏幕截图.png")

```java
        new SwitchListTile(
          secondary: const Icon(Icons.shutter_speed),
          title: const Text('硬件加速'),
          value: _switchSelected,
          onChanged: (bool value) {
            setState(() {
              _switchSelected = !_switchSelected;
            });
          },
        ),
```


<hr>

### Checkbox

![输入图片说明](https://images.gitee.com/uploads/images/2019/0623/182624_7fae266a_568055.png "屏幕截图.png")

```java
    Checkbox(
          value: _checkboxSelected,
          //选中时的颜色
          activeColor: Colors.red,
          onChanged: (value) {
            setState(() {
              _checkboxSelected = value;
            });
          },
        )

```

### CheckboxListTile

![输入图片说明](https://images.gitee.com/uploads/images/2019/0623/182650_04a38aa4_568055.png "屏幕截图.png")

```java 

      CheckboxListTile(
          secondary: const Icon(Icons.shutter_speed),
          title: const Text('硬件加速'),
          value: _checkboxSelected,
          onChanged: (bool value) {
            setState(() {
              _checkboxSelected = !_checkboxSelected;
            });
          },
        ),

```
