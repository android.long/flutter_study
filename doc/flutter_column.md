# 水平和垂直布局详解

在原生Android开发中，我们经常会用LinearLayout来达到水平或垂直方向的布局，
在原生IOS 开发中，水平或垂直方向的布局，只需要通过计算位置，通过设置frame来实现，
在Flutter中使用Row和Column组件来实现水平或垂直方向的布局，Row组件主要功能是处理水平方向的布局，Column组件主要功能是处理垂直方向的布局。

Android原生 | Flutter
------- | -------
LinearLayout 里面的android:orientation="horizontal” | Row组件
LinearLayout 里面的 android:orientation="vertical” | Column组件

### 1 基本垂直布局

垂直方向排列 a b c 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0623/225601_c4e5e9f8_568055.png "屏幕截图.png")

```
  Widget buildView() {
    return Container(
      color: Colors.white,
      child: Column(
        children: <Widget>[
          Text("a"),
          Text("b"),
          Text("c"),
        ],
      )
    );
  }

```

### 2 基本水平布局

水平方向排列 a b c 

![输入图片说明](https://images.gitee.com/uploads/images/2019/0623/225627_5786b4c8_568055.png "屏幕截图.png")

```
  Widget buildView() {
    return Container(
      color: Colors.white,
      child: Row(
        children: <Widget>[
          Text("a"),
          Text("b"),
          Text("c"),
        ],
      )
    );
  }
```


### 3 布局中的自适应与填充


#### 3.1水平方向 的填充，垂直方向的自适应 

对于Android 原生来讲 LinearLayout android:orientation="horizontal"

```
//水平方向 的填充
android:layout_width="match_parent"
android:layout_height="wrap_content"

```
对就 Flutter 中的 Row 

```
mainAxisSize: MainAxisSize.max,
```

#### 3.2垂直方向 的填充，水平方向的自适应 

对于Android 原生来讲 LinearLayout android:orientation="vertical"

```
//垂直方向 的填充
android:layout_width="wrap_content"
android:layout_height="match_parent"

```
对就 Flutter 中的 Column 

```
mainAxisSize: MainAxisSize.max,
```

#### 3.3 自适应

对于Android 原生来讲 LinearLayout

```
android:layout_width="wrap_content"
android:layout_height="wrap_content"

```
垂直方向 水平方向 全部是 自适应的效果 对就 Flutter 中 Column Row 
```
mainAxisSize: MainAxisSize.min,
```


### 4 布局中的子组件的对齐

在Android 中，LinearLayout通过 gravity属性来规范 子View的对齐方式，
在Flutter中我们可以使用 Row/Column组件 的 MainAxisAlignment 以及 CrossAxisAlignment 实现控件的对齐效果。

Row 的主轴为水平方向,那么它的交叉轴为垂直方向.
Column 的主轴为垂直方向,那么它的交叉轴为水平方向.

#### 4.1 MainAxisAlignment是设置主轴的对齐方式

MainAxisAlignment 有6个属性值，默认值为start

```
//将子控件放在主轴的起始位置。
1、MainAxisAlignment.start，
//将子控件放在主轴末尾。
2、MainAxisAlignment.end，
//将子控件放在主轴中间位置。
3、MainAxisAlignment.center，
// 将主轴方向上的空白区域等分，使得子控件之间的空白区域相等，
// 两端的子控件都靠近首尾，没有间隙。
4、MainAxisAlignment.spaceBetween 

// 将主轴方向上的空白区域等分，使得子控件之间的空白区域相等，
// 两端的子控件都靠近首尾，首尾子控件的空白区域为1/2。
5、MainAxisAlignment.spaceAround 

// 将主轴方向上的空白区域等分，使得子控件之间的空白区域相等，包括首尾。
6、MainAxisAlignment.spaceEvenly
```
#### 4.2 CrossAxisAlignment是设置交叉轴方向的对齐方式

CrossAxisAlignment 有5个属性值，默认值为center，分别是：

```
CrossAxisAlignment.start, 子孩子控件显示在交叉轴的起始位置。
CrossAxisAlignment.end, 子孩子控件显示在交叉轴的末尾位置。
CrossAxisAlignment.center, 子孩子控件显示在交叉轴的中间位置。
CrossAxisAlignment.stretch, 子孩子控件完全填充交叉轴方向的空间。
CrossAxisAlignment.baseline, 让子孩子控件的baseline在交叉轴方向对齐。
```
 


### 5 子布局的权重比例设置 

在Android中 layout_weight 是LinearLayout里面的属性，它是用来给子布局设置权重的，表示给布局按照设置的比例去分配空间。

在Flutter中我们可以使用 Expanded 包裹**Row**或**Column**组件,使用 Expanded 组件里面的 flex**属性** 去实现同样的效果。

```
  Widget buildTwoRow() {
    return Row(
      //水平方向填充
      mainAxisSize: MainAxisSize.max,
      //平分空白
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(color: Colors.grey,height: 120,),
        ),
        Expanded(
          flex: 1,
          child: Container(color: Colors.green,height: 120,),
        ),
        Expanded(
          flex: 1,
          child: Container(color: Colors.blue,height: 120,),
        ),
      ],
    );
  }
```
在水平方向上，三个子View 平均分配空间

![输入图片说明](https://images.gitee.com/uploads/images/2019/0623/232812_bdb4fdd6_568055.png "屏幕截图.png")

### 6 布局中的子 View 的排列顺序

在Flutter中的Row或Column中关于 direction ，

textDirection 的属性值为 TextDirection.ltr，表示所有的子控件都是从左到右顺序排列，这是默认值。如果为TextDirection.rtl 表示从右边开始向左边倒序排列。

verticalDirection 的属性值为 VerticalDirection.down, 表示所有的子控件都是从上到下顺序排列。如果为VerticalDirection.up 从底部开始向上倒序排列。
