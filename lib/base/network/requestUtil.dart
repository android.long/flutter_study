import 'dart:async';

import 'package:rxdart/rxdart.dart';

import 'api.dart';
import 'bean/login_response_entity.dart';
import 'intercept/base_intercept.dart';

class RequestMap {
  static PublishSubject<LoginResponseEntity> requestLogin<BaseResponse>(
      BaseIntercept baseIntercept) {
    String url = "xiandu/category/wow";
    LoginResponseEntity loginResponseEntity =
        LoginResponseEntity(); //模拟 带参数，直接对象转json就可以了
    loginResponseEntity.error = false;
    return HttpManager().get<LoginResponseEntity>(url,
        queryParameters: loginResponseEntity.toJson(),
        baseIntercept: baseIntercept,
        isCancelable: false);
  }

  static PublishSubject testErrorrequest(BaseIntercept baseIntercept) {
    String urlError = "error";
    return HttpManager().get<LoginResponseEntity>(urlError = "error",
        baseIntercept: baseIntercept);
  }
}
