import 'package:flutter/material.dart';
import 'package:flutter_study_test/test/page/DemoPage.dart';
import 'package:flutter_study_test/test/widget/DemoStateFulWidget.dart';
import 'IndexPage.dart';

//Flutter程序入口
void main() => runApp(getApp());

Widget getApp() {
  return new MaterialApp(
    //应用默认所显示的界面 页面
    home:   IndexPage()
  );
}
