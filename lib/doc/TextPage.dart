import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class TextPage extends StatefulWidget {
  @override
  TextPagePageState createState() => TextPagePageState();
}

class TextPagePageState extends State<TextPage> {
  @override
  Widget build(BuildContext context) {
    return buildPage();
  }

  //构造页面主体
  Widget buildPage() {
    return new Scaffold(
      //显示在界面顶部的一个 AppBar
      appBar: buildDefaultBar("test"),
      //body：当前界面所显示的主要内容
      body: new Container(color: Colors.grey),
    );
  }

  //构造 AppBar
  Widget buildDefaultBar(String title) {
    return AppBar(
      //标题居中显示
      centerTitle: true,
      //返回按钮占位
      leading: Container(),
      //标题显示
      title: Text(title),
    );
  }
}
