import 'package:flutter/material.dart';

//Flutter程序入口
void main() => runApp(getApp());

Widget getApp() {
  return new MaterialApp(
    //应用默认所显示的界面 页面
    home: getAllScaffold(),
  );
}

Widget getDefaultScaffold() {
  return new Scaffold(
    //显示在界面顶部的一个 AppBar
    appBar: buildDefaultBar("test"),
    //body：当前界面所显示的主要内容
    body: new Container(color: Colors.grey),
  );
}

/**
 * 所有的属性设置
 */
Widget getAllScaffold() {
  return new Scaffold(
    //显示在界面顶部的一个 AppBar
    appBar: buildDefaultBar("test"),
    //body：当前界面所显示的主要内容
    body: new Container(color: Colors.grey),
    //显示在页面底部的导航栏
    bottomNavigationBar: buildBottomBar(),
    //固定在下方显示的按钮，比如对话框下方的确定、取消按钮。
    //一般不使用
//    persistentFooterButtons: <Widget>[
//      new Icon(Icons.share),
//      new Icon(Icons.shop),
//    ],
    //侧边栏
    drawer: new Drawer(
      child: new Container(
        color: Colors.green,
      ),
    ),
    //悬浮按钮
    floatingActionButton: buildFloatingAction(),
  );
}

Widget buildFloatingAction() {
  return new Builder(builder: (BuildContext context) {
    return new FloatingActionButton(
        tooltip: '这里是长按提示的文字',
        backgroundColor: Colors.red,
        //设置悬浮按钮的背景颜色
//             heroTag: ,//页面切换动画的tag
        elevation: 10.0,
        //阴影大小
//             highlightElevation: 20.0,//设置高亮的阴影
        mini: false,
        //设置是否为小图标
        child: new Icon(Icons.access_alarm),
        onPressed: () {});
  });
}

//底部导航栏
Widget buildBottomBar() {
  return new BottomNavigationBar(
    //底部导航栏
    currentIndex: 1, //默认选中的位置
    fixedColor: Colors.green, //选中的颜色
    items: <BottomNavigationBarItem>[
      new BottomNavigationBarItem(
        icon: new Icon(
          Icons.airplay,
        ),
        title: new Text(
          '主页',
        ),
      ),
      new BottomNavigationBarItem(
        icon: new Icon(
          Icons.add,
        ),
        title: new Text(
          '加量',
        ),
      ),
      new BottomNavigationBarItem(
        icon: new Icon(
          Icons.account_box,
        ),
        title: new Text(
          '个人中心',
        ),
      ),
    ],
  );
}

//构造 AppBar
Widget buildDefaultBar(String title) {
  return AppBar(
    //标题居中显示
    centerTitle: true,
    //返回按钮占位
    leading: Container(),
    //标题显示
    title: Text(title),
  );
}
