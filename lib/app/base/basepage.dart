import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'LogHelper.dart';

abstract class BasePage<T extends StatefulWidget> extends State{

  //底部tab 文字默认显示颜色
  final tabTextStyleNormal = new TextStyle(color: const Color(0xff969696));
  //底部tab 文字选中显示颜色
  final tabTextStyleSelected = new TextStyle(color: const Color(0xff63ca6c));

  FlatButton buildFlatButton(String text,@required VoidCallback onPressed){
    return FlatButton(
      color: Colors.blue,
      highlightColor: Colors.blue[700],
      colorBrightness: Brightness.dark,
      splashColor: Colors.grey,
      child: Text(text),
      shape:RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      onPressed:onPressed,
    );
  }

}

