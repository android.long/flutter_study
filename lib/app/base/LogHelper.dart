
class LogHelper {
  static bool isLog = true;

  static LogHelper _instance;

  factory LogHelper() {
    _instance ??= LogHelper();
    return _instance;
  }

 static void info(String msg) {
    if (isLog) print(msg);
  }
}
