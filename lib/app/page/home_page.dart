import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_study_test/app/base/LogHelper.dart';
import 'package:flutter_study_test/app/base/basepage.dart';

import 'home_item_main.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new MyOSCClientState();
}

class MyOSCClientState extends BasePage<HomePage> {

  //当前显示的页面index
  int _tabIndex = 0;
  //底部显示的tab image
  var tabImages;
  //tab页面
  var _body;
  //底部显示的tab
  var appBarTitles = ['资讯', '动弹', '发现', '我的'];
  //底部tab 文字默认显示颜色
  final tabTextStyleNormal = new TextStyle(color: const Color(0xff969696));
  //底部tab 文字选中显示颜色
  final tabTextStyleSelected = new TextStyle(color: const Color(0xff63ca6c));




  List<BottomNavigationBarItem> getBarItems() {
    List<BottomNavigationBarItem> items = new List();
    for (int i = 0; i < appBarTitles.length; i++) {
      items.add(new BottomNavigationBarItem(
          icon: getTabIcon(i), title: getTabTitle(i)));
    }
    return items;
  }

  void initData() {
    if (tabImages == null) {
      tabImages = [
        [
          getTabImage('images/ic_nav_news_normal.png'),
          getTabImage('images/ic_nav_news_actived.png')
        ],
        [
          getTabImage('images/ic_nav_tweet_normal.png'),
          getTabImage('images/ic_nav_tweet_actived.png')
        ],
        [
          getTabImage('images/ic_nav_discover_normal.png'),
          getTabImage('images/ic_nav_discover_actived.png')
        ],
        [
          getTabImage('images/ic_nav_my_normal.png'),
          getTabImage('images/ic_nav_my_pressed.png')
        ]
      ];
    }
    _body = new IndexedStack(
      children: <Widget>[
        new MainPage(),
//        new TweetsListPage(),
//        new DiscoveryPage(),
//        new MyInfoPage()
      ],
      index: _tabIndex,
    );
  }

  TextStyle getTabTextStyle(int curIndex) {
    if (curIndex == _tabIndex) {
      return tabTextStyleSelected;
    }
    return tabTextStyleNormal;
  }
  Image getTabImage(path) {
    return new Image.asset(path, width: 20.0, height: 20.0);
  }
  Image getTabIcon(int curIndex) {
    if (curIndex == _tabIndex) {
      return tabImages[curIndex][1];
    }
    return tabImages[curIndex][0];
  }

  Text getTabTitle(int curIndex) {
    return new Text(appBarTitles[curIndex], style: getTabTextStyle(curIndex));
  }

  @override
  Widget build(BuildContext context) {
    initData();
    return new MaterialApp(
      home: new Scaffold(
//        appBar: new AppBar(
//            title: new Text(appBarTitles[_tabIndex], style: new TextStyle(color: Colors.white)),
//            iconTheme: new IconThemeData(color: Colors.white)
//        ),
        floatingActionButton: FloatingActionButton(
            onPressed: () {},
            child: Icon(
              Icons.add,
              color: Colors.white,
            )),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        backgroundColor: Colors.white,
        body: _body,
        bottomNavigationBar: BottomAppBar(
          color: Colors.white,
          shape: CircularNotchedRectangle(),
          child: Row(
            children: [
              IconButton(
                icon: Icon(Icons.home),
                onPressed: () {
                  setState(() {
                    _tabIndex = 0;
                  });
                },
              ),
              SizedBox(), //中间位置空出
              IconButton(
                icon: Icon(Icons.people),
                onPressed: () {
                  setState(() {
                    _tabIndex = 1;
                  });
                },
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceAround, //均分底部导航栏横向空间
          ),
//          child: new CupertinoTabBar(
//            items: getBarItems(),
//            currentIndex: _tabIndex,
//            onTap: (index) {
//              setState(() {
//                _tabIndex = index;
//                LogHelper.info("ontap " + index.toString());
//              });
//            },
//          ),
        ),
        //drawer: new MyDrawer(),
      ),
    );
  }
}
