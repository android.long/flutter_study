import 'package:base_library/base_library.dart';
import 'package:fluintl/fluintl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_study_test/app/res/strings.dart';
import 'package:flutter_study_test/app/utils/navigator_util.dart';
import 'package:flutter_study_test/app/utils/utils.dart';

import 'home_item_item_page.dart';

class HeaderItemView {
  final String labelId;

  HeaderItemView(this.labelId);
}

final List<HeaderItemView> _allPages = <HeaderItemView>[
  new HeaderItemView(Ids.titleHome),
  new HeaderItemView(Ids.titleRepos),
  new HeaderItemView(Ids.titleEvents),
  new HeaderItemView(Ids.titleSystem),
];

/**
 * Container 官方给出的简介，是一个结合了绘制（painting）、定位（positioning）以及尺寸（sizing）widget的widget。
 */
class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    LogUtil.e("MainPagess build......");
    return new DefaultTabController(
        length: _allPages.length,
        child: new Scaffold(
          appBar: new MyAppBar(
            leading: new Container(
              //嵌套一层设置内边距
              padding: const EdgeInsets.all(8.0),
              child: new Container(
                //color：用来设置container背景色，如果foregroundDecoration设置的话，可能会遮盖color效果
                //foregroundDecoration：绘制在child前面的装饰。
                //width：container的宽度，设置为double.infinity可以强制在宽度上撑满，不设置，则根据child和父节点两者一起布局。
                //height：container的高度，设置为double.infinity可以强制在高度上撑满。
                //constraints：添加到child上额外的约束条件。
                //margin：围绕在decoration和child之外的空白区域，不属于内容区域。

                //绘制在child后面的装饰，设置了decoration的话，就不能设置color属性，否则会报错，此时应该在decoration中进行颜色的设置
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                    image: AssetImage(
                      Utils.getImgPath('ali_connors'),
                    ),
                  ),
                ),
              ),
            ),
            //标题是否居中显示，默认值根据不同的操作系统，显示方式不一样,true居中 false居左
            centerTitle: true,
            //Toolbar 中主要内容，通常显示为当前界面的标题文字
            title: new TabLayout(),
            //一个 Widget 列表，代表 Toolbar 中所显示的菜单，对于常用的菜单，通常使用 IconButton 来表示；
            // 对于不常用的菜单通常使用 PopupMenuButton 来显示为三个点，点击后弹出二级菜单
            actions: <Widget>[
              new IconButton(
                  icon: new Icon(Icons.search),
                  onPressed: () {
                    LogUtil.v("search click ");
//                    NavigatorUtil.pushPage(context, new SearchPage(),
//                        pageName: "SearchPage");
                    // NavigatorUtil.pushPage(context,  new TestPage());
                    //  NavigatorUtil.pushPage(context,  new DemoApp());
                  })
            ],
          ),
          body: new TabBarViewLayout(),
//          drawer: new Drawer(
//            child: new MainLeftPage(),
//          ),
        ));
  }
}

class TabLayout extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new TabBar(
      //是否可滚动
      isScrollable: true,
      ////每个label的padding值
      labelPadding: EdgeInsets.all(12.0),
      ////指示器大小计算方式，TabBarIndicatorSize.label跟文字等宽,TabBarIndicatorSize.tab跟每个tab等宽
      indicatorSize: TabBarIndicatorSize.label,
      tabs: _allPages
          .map((HeaderItemView page) => new Tab(text: page.labelId))
          .toList(),
    );
  }
}

class TabBarViewLayout extends StatelessWidget {
  Widget buildTabView(BuildContext context, HeaderItemView page) {
    String labelId = page.labelId;
    switch (labelId) {
      case Ids.titleHome:
        return HomePage(labelId: labelId);
        break;
      case Ids.titleRepos:
        return HomePage(labelId: labelId);
        break;
      case Ids.titleEvents:
        return HomePage(labelId: labelId);
        break;
      case Ids.titleSystem:
        return HomePage(labelId: labelId);
        break;
      default:
        return Container();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    LogUtil.e("TabBarViewLayout build.......");
    return new TabBarView(
        children: _allPages.map((HeaderItemView page) {
      return buildTabView(context, page);
    }).toList());
  }
}
