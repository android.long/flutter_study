import 'package:base_library/base_library.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_study_test/test/page/DemoPage.dart';
import 'package:flutter_study_test/test/widget/DEMOWidget.dart';
import 'package:flutter_study_test/test/widget/DemoStateFulWidget.dart';
import 'package:flutter_study_test/test/widget/IndexPage.dart';
import 'package:shared_preferences/shared_preferences.dart';

/**
 * 在 Flutter 中，一切的显示都是 Widget 。Widget 是一切的基础，作为响应式的渲染，属于 MVVM 的实现机制。
 * Widget 分为 有状态 和 无状态 两种，在 Flutter 中每个页面都是一帧。无状态就是保持在那一帧。而有状态的 Widget 当数据更新时，其实是绘制了新的 Widget，只是 State 实现了跨帧的数据同步保存
 * StatefulWidget，通过调用setState({})方法来刷新控件
 */
class TestApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TestAppState();
  }
}

class TestAppState extends State<TestApp> {
  Locale _locale;
  Color _themeColor = Colours.app_main;
  @override
  void initState() {
    super.initState();

  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {

    return getMteriaAPP();
  }


  /**
   * Flutter 中除了布局的 Widget，还有交互显示的 Widget 和完整页面呈现的Widget。
   * 其中常见的有 MaterialApp、Scaffold、Appbar、Text、Image、FlatButton等。下面简单介绍这些 Wdiget，并完成一个页面。
   */

  /**
   * MaterialApp 一般作为APP顶层的主页入口，可配置主题，多语言，路由等
   * Scaffold	一般用户页面的承载Widget，包含appbar、snackbar、drawer等material design的设定。
   * Appbar	一般用于Scaffold的appbar ，内有标题，二级页面返回按键等，当然不止这些，tabbar等也会需要它 。
   * Text	显示文本，几乎都会用到，主要是通过style设置TextStyle来设置字体样式等。
   * RichText	富文本，通过设置TextSpan，可以拼接出富文本场景。
   * TextField	文本输入框 ：new TextField(controller: //文本控制器, obscureText: "hint文本");
   * Image	图片加载: new FadeInImage.assetNetwork( placeholder: "预览图", fit: BoxFit.fitWidth, image: "url");
   * FlatButton	按键点击: new FlatButton(onPressed: () {},child: new Container());
   */
  getMteriaAPP(){
    return new MaterialApp(
      //应用默认所显示的界面 页面
      home: new IndexPage(),
      //应用的顶级导航表格，多页面应用 控制页面跳转
      //routes: ,
       // 当系统修改语言的时候，会触发å这个回调
      //onLocaleChanged ：
      // 应用各种 UI 所使用的主题颜色
      theme: ThemeData.light().copyWith(
        primaryColor: _themeColor,
        accentColor: _themeColor,
        indicatorColor: Colors.white,

      ),
      locale: _locale,

    );
  }

}