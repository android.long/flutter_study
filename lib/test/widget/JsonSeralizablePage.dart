import 'package:base_library/base_library.dart';
import 'package:flutter/material.dart';
import 'package:flutter_study_test/app/view/header_item.dart';
import 'package:flutter_study_test/app/view/repos_item.dart';
import 'package:flutter_study_test/app/view/widget_progress.dart';
import 'package:flutter_study_test/test/bean/UserBean.dart';
import 'package:flutter_study_test/test/http/NetUtil.dart';
import 'package:flutter_study_test/test/view/repos_item.dart';
import 'package:fluttertoast/fluttertoast.dart';

class JsonSeralizablePage extends StatefulWidget {
  @override
  _JsonSeralizablePageState createState() => _JsonSeralizablePageState();
}

class _JsonSeralizablePageState extends State<JsonSeralizablePage> {
  String name = "";
  String email = "";
  List <UserBean> userList = new List();
  ScrollController _scrollController = ScrollController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("JSON"),
      ),
      body: (userList != null && userList.length == 0)
          ? ProgressView()
          :buildRepos(context, userList),


    );
  }
  _onRefresh(){

  }
  Widget buildRepos(BuildContext context, List<UserBean> list) {
    if (ObjectUtil.isEmpty(list)) {
      return new Container(height: 0.0);
    }
    List<Widget> _children = list.map((model) {
      return new UserBeanItem(
        model,
        isHome: true,
      );
    }).toList();
    List<Widget> children = new List();
    children.add(new HeaderItem(
      leftIcon: Icons.book,
      title: "推荐项目",
      onTap: () {
        LogUtil.e("推荐项目: 更多" );
//        NavigatorUtil.pushTabPage(context,
//            labelId: Ids.titleReposTree, titleId: Ids.titleReposTree);
      },
    ));
    children.addAll(_children);
    return SingleChildScrollView(
      child:  new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: children,
      ),
    );
  }
  Widget _renderRow(BuildContext context, int index) {
      return ListTile(
        title: Text(userList[index].name),
      );

  }
  @override
  void initState() {
    super.initState();

    getContent();
  }

  //获取网络数据
  void getContent() {
    NetUtil.get("10001", (data) {
      List list = data;

      for (int i=0;i<list.length;i++){
        UserBean user = UserBean(data[i]);
        userList.add(user);
      }

      setState(() {

      });
    }, errorCallBack: (errorMsg) {
      print("error:" + errorMsg);
      Fluttertoast.showToast(msg: "错误:"+errorMsg);
    });
  }
}