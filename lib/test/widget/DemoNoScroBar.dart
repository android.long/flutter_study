import 'dart:async';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_study_test/app/page/home_item_main.dart';

/**
 * 有状态StatefulWidget
 *  继承于 StatefulWidget，通过 State 的 build 方法去构建控件
 */
class DemoNoScroBar extends StatefulWidget {
  ////通过构造方法传值
  DemoNoScroBar();

  //主要是负责创建state
  @override
  _DemoStateWidgetState createState() => _DemoStateWidgetState();
}

/**
 * 在 State 中,可以动态改变数据
 * 在 setState 之后，改变的数据会触发 Widget 重新构建刷新
 */
class _DemoStateWidgetState extends State<DemoNoScroBar>
    with SingleTickerProviderStateMixin {

  _DemoStateWidgetState();

  @override
  void initState() {
    ///初始化，这个函数在生命周期中只调用一次
    super.initState();
  }

  @override
  void didChangeDependencies() {
    ///在initState之后调 Called when a dependency of this [State] object changes.
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return buildTabScaffold();
  }

  Widget buildTabScaffold() {
    String str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    return  SingleChildScrollView(
        scrollDirection: Axis.vertical,
        reverse: true,
        padding: EdgeInsets.all(0.0),
        physics: BouncingScrollPhysics(),
        child: Center(
          child: Column(
            //动态创建一个List<Widget>
            children: str.split("")
            //每一个字母都用一个Text显示,字体为原来的两倍
                .map((c) => Text(c, textScaleFactor: 2.0))
                .toList(),
          ),
        ),
    );
  }

  @override
  void dispose() {
    ///销毁
    super.dispose();
  }
}

