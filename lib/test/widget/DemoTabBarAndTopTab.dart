import 'dart:async';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_study_test/app/page/home_item_main.dart';

/**
 * 有状态StatefulWidget
 *  继承于 StatefulWidget，通过 State 的 build 方法去构建控件
 */
class TabBarAndTopTab extends StatefulWidget {
  ////通过构造方法传值
  TabBarAndTopTab();

  //主要是负责创建state
  @override
  _DemoStateWidgetState createState() => _DemoStateWidgetState();
}

/**
 * 在 State 中,可以动态改变数据
 * 在 setState 之后，改变的数据会触发 Widget 重新构建刷新
 */
class _DemoStateWidgetState extends State<TabBarAndTopTab>
    with SingleTickerProviderStateMixin {


  _DemoStateWidgetState();

  @override
  void initState() {
    ///初始化，这个函数在生命周期中只调用一次
    super.initState();
  }

  @override
  void didChangeDependencies() {
    ///在initState之后调 Called when a dependency of this [State] object changes.
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return buildTabScaffold();
  }

  //通过“bottom”属性来添加一个导航栏底部tab按钮组，将要实现的效果如下：
  Widget buildTabScaffold() {
    List tabs = ["首页", "发现", "我的", "设置"];
    //用于控制/监听Tab菜单切换
    //TabBar和TabBarView正是通过同一个controller来实现菜单切换和滑动状态同步的。
    TabController tabController =
        TabController(length: tabs.length, vsync: this);
    return Scaffold(
        appBar: AppBar(
          title: Text('标题'),
          bottom: TabBar(
              //生成Tab菜单
              controller: tabController,
              tabs: tabs.map((e) => Tab(text: e)).toList()),
          //设置标题居中
          centerTitle: true,
          //设置最左侧按钮
          leading: Builder(builder: (context) {
            return IconButton(
              icon: Icon(Icons.bug_report, color: Colors.white),
              onPressed: () {
                // 打开抽屉菜单
                Scaffold.of(context).openDrawer();
              },
            );
          }),
          actions: <Widget>[
            //导航栏右侧菜单
            IconButton(icon: Icon(Icons.search), onPressed: () {}),
          ],
        ),
        backgroundColor: Colors.white,
        body: TabBarView(
          controller: tabController,
          //创建Tab页
          children: tabs.map((e) {
            return Container(
              alignment: Alignment.center,
              child: Text(e, textScaleFactor: 1),
            );
          }).toList(),
        ),
    );
  }

  @override
  void dispose() {
    ///销毁
    super.dispose();
  }
}

