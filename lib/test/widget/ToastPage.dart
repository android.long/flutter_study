import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ToastPage extends StatefulWidget {
  @override
  ToastPageState createState() {
    return ToastPageState();
  }
}

class ToastPageState extends State<ToastPage> {

  showErrToast(){
    Fluttertoast.showToast(
        msg: "This is Center Short Toast",
        //显示时间 与android 中的一样
        toastLength: Toast.LENGTH_SHORT,
        //显示位置
        gravity: ToastGravity.CENTER,
        timeInSecForIos: 1,
        //显示背景颜色
        backgroundColor: Colors.red,
        //显示文字颜色
        textColor: Colors.white,
        //字体大小
        fontSize: 16.0
    );
  }

  showToast(){
    Fluttertoast.showToast(msg: "show msg");
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ToastPage'),
      ),
      body: Column(children: <Widget>[
        RaisedButton(
          child: Text(" show err toast "),
          onPressed: () {
            showErrToast();
          },
        ),
        RaisedButton(
          child: Text(" show toast "),
          onPressed: () {
            showToast();
          },
        )
      ]),
    );
  }
}
