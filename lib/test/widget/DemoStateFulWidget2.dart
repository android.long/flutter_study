import 'dart:async';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_study_test/app/page/home_item_main.dart';

/**
 * 有状态StatefulWidget
 *  继承于 StatefulWidget，通过 State 的 build 方法去构建控件
 */
class DemoStateFulWidget2 extends StatefulWidget {

  ////通过构造方法传值
  DemoStateFulWidget2();
  //主要是负责创建state
  @override
  _DemoStateWidgetState createState() => _DemoStateWidgetState();
}

/**
 * 在 State 中,可以动态改变数据
 * 在 setState 之后，改变的数据会触发 Widget 重新构建刷新
 */
class _DemoStateWidgetState extends State<DemoStateFulWidget2>
    with SingleTickerProviderStateMixin {
  _DemoStateWidgetState();

  @override
  void initState() {
    ///初始化，这个函数在生命周期中只调用一次
    super.initState();
    initTabData();
  }

  @override
  void didChangeDependencies() {
    ///在initState之后调 Called when a dependency of this [State] object changes.
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    //构建页面
    return buildBottomTabScaffold();
  }

  var _tabIndex = 0;
  List<String> pageTitleList = ["首页", "我的"];
  List<Widget> pageList = new List();
  List<Widget> bottomBarList = new List();

  void initTabData() {
    for(int i=0;i<pageTitleList.length;i++){
      pageList.add(new EachView(pageTitleList[i]));
      bottomBarList.add(new EachView(pageTitleList[i]));
    }
  }

  Widget buildBottomTabScaffold() {
    //数组索引，通过改变索引值改变视图
    return new Scaffold(
      //圆形悬浮+号
      floatingActionButton: FloatingActionButton(
          onPressed: () {},
          child: Icon(
            Icons.add,
            color: Colors.white,
          )),
      //BottomAppBar 与 FloatingActionButton 结合使用 内陷效果
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      //背景
      backgroundColor: Colors.white,
      //BottomAppBar 中对应的 Tabdmdm
      body: new IndexedStack(
        children:bottomBarList,
        index: _tabIndex,
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        shape: CircularNotchedRectangle(),
        child: Row(
          children: [
            IconButton(
              icon: Icon(Icons.home),
              onPressed: () {
                setState(() {
                  _tabIndex = 0;
                });
              },
            ),
            SizedBox(), //中间位置空出
            IconButton(
              icon: Icon(Icons.people),
              onPressed: () {
                setState(() {
                  _tabIndex = 1;
                });
              },
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.spaceAround, //均分底部导航栏横向空间
        ),
      ),
      //drawer: new MyDrawer(),
    );
  }
  @override
  void dispose() {
    ///销毁
    super.dispose();
  }
}

//子页面
//代码中设置了一个内部的_title变量，这个变量是从主页面传递过来的，然后根据传递过来的具体值显示在APP的标题栏和屏幕中间。
class EachView extends StatefulWidget {
  String _title;

  EachView(this._title);

  @override
  _EachViewState createState() => _EachViewState();
}

class _EachViewState extends State<EachView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget._title)),
      body: Center(child: Text(widget._title)),
    );
  }
}
