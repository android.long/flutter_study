import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

class SwiperPage extends StatefulWidget {
  @override
  SwiperPageState createState() {
    return SwiperPageState();
  }
}

class SwiperPageState extends State<SwiperPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('轮播组件'),
      ),
      body: Column(
        children: <Widget>[
          Container(
            height: 200.0,
            child: buildStyle1(),
          ),
          Padding(
            padding: EdgeInsets.only(top: 20),
          ),
          Container(
            height: 200.0,
            child: buildStyle2(),
          )
        ],
      ),
    );
  }

  Widget _swiperBuilder(BuildContext context, int index) {
    return (Image.network(
      "http://hbimg.b0.upaiyun.com/a3e592c653ea46adfe1809e35cd7bc58508a6cb94307-aaO54C_fw658",
      fit: BoxFit.fill,
    ));
  }

  Widget buildStyle1() {
    return Swiper(
      //条目布局
      itemBuilder: _swiperBuilder,
      //条目个数据
      itemCount: 3,
      //展示默认分页指示器
      pagination: new SwiperPagination(
          //如果要将分页指示器放到其他位置，那么可以修改这个参数
          //alignment: ,
          //分页指示器与容器边框的距离
          margin: EdgeInsets.all(10.0),
          //目前已经定义了两个默认的分页指示器样式： SwiperPagination.dots 、 SwiperPagination.fraction
          builder: DotSwiperPaginationBuilder(
            color: Colors.black54,
            activeColor: Colors.white,
          )),
      //展示默认分页按钮
      control: new SwiperControl(),
      //滚动方向，设置为Axis.vertical如果需要垂直滚动
      scrollDirection: Axis.horizontal,
      //无限轮播模式开关
      loop: true,
      //初始的时候下标位置
      index: 0,
      //自动播放开关.
      autoplay: true,
      //自动播放延迟毫秒数.
      autoplayDelay: 3000,
      //当用户拖拽的时候，是否停止自动播放.
      autoplayDisableOnInteraction: false,
      //动画时间，单位是毫秒
      duration: 3000,
      //当用户点击某个轮播的时候调用
      onTap: (index) => print('点击了第$index个'),
    );
  }

  Widget buildStyle2() {
    return new Swiper(
      itemBuilder: (BuildContext context, int index) {
        return new Image.network(
          "http://hbimg.b0.upaiyun.com/a3e592c653ea46adfe1809e35cd7bc58508a6cb94307-aaO54C_fw658",
          fit: BoxFit.fill,
        );
      },
      itemCount: 10,
      viewportFraction: 0.8,
      scale: 0.9,
    );
  }
}
