import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_study_test/app/page/home_item_main.dart';

/**
 * 有状态StatefulWidget
 *  继承于 StatefulWidget，通过 State 的 build 方法去构建控件
 */
class DemoStateFulWidget extends StatefulWidget {
  final String text;

  ////通过构造方法传值
  DemoStateFulWidget(this.text);

  //主要是负责创建state
  @override
  _DemoStateWidgetState createState() => _DemoStateWidgetState(text);
}

/**
 * 在 State 中,可以动态改变数据
 * 在 setState 之后，改变的数据会触发 Widget 重新构建刷新
 */
class _DemoStateWidgetState extends State<DemoStateFulWidget>
    with SingleTickerProviderStateMixin {
  String text;

  _DemoStateWidgetState(this.text);

  @override
  void initState() {
    ///初始化，这个函数在生命周期中只调用一次
    super.initState();
    initTabData();
  }

  @override
  void didChangeDependencies() {
    ///在initState之后调 Called when a dependency of this [State] object changes.
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    Container(
      color: Colors.grey,
      child: Text(text ?? "这就是有状态DMEO"),
    );

    /**
     * 最常用的默认布局！只能包含一个child:，支持配置 padding,margin,color,宽高,decoration（一般配置边框和阴影）等配置，
     * 在 Flutter 中，不是所有的控件都有 宽高、padding、margin、color 等属性，所以才会有 Padding、Center 等 Widget 的存在。
     */
    var testContainer = new Container(
        // color 与 decoration 不可同时设置
        //color: Colors.white,
        //四周10大小的maring
        margin: EdgeInsets.all(10.0),
        height: 120.0,
        width: 500.0,

        ///透明黑色遮罩
        decoration: new BoxDecoration(

            ///弧度为4.0
            borderRadius: BorderRadius.all(Radius.circular(4.0)),
            //设置了decoration的color，就不能设置Container的color。
            color: Colors.black,

            ///边框
            border: new Border.all(color: Colors.red, width: 0.3)),
        child: new Text("666666"));
    var testColorContainer = new Container(
      // color 与 decoration 不可同时设置
      color: Colors.white,
      //外间距
      margin: EdgeInsets.all(10.0),
      //内间距
      padding: EdgeInsets.all(10.0),
      height: 120.0,
      width: 500.0,
      child: new Text("666666"),
    );

    // Padding 只有一个子 Widget。只用于设置Padding，常用于嵌套child，给child设置padding。
    // Center 只有一个子 Widget。只用于居中显示，常用于嵌套child，给child设置居中。
    var testPadingContainer = new Container(
      color: Colors.white,
      child: Padding(
        //上下左右内边距 40像素
        padding: EdgeInsets.all(40.0),
        //子view为嵌套一个Container
        child: Container(
          //颜色为蓝色
          color: Colors.blue,
          //将子布局中的 View居中显示
          child: Center(
            //Center 中只能有一个View
            child: Text("居中view"),
          ),
        ),
      ),
    );

    //Stack 可以有多个子 Widget。 子Widget堆叠在一起。类似android 中的framelayout
    var testStack = new Stack(
      //设置居中对齐所有的子Widget  默认为 AlignmentDirectional.topStart 左上角对齐
      alignment: AlignmentDirectional.center,
      //所有的子 Widget
      children: <Widget>[
        Container(
          color: Colors.red,
        ),
        //后面的widget会浮在前面的View上面
        Container(
          color: Colors.blue,
          width: 100.0,
          height: 150.0,
        ),
      ],
    );

    //Column、Row 绝对是必备布局， 横竖布局也是日常中最常见的场景
    // 构造 Android Java 语言中 LinearLayout orientation="vertical" 中的垂直布局
    var cloumn1 = new Column(
      //即是竖直向居中（官方描述为 主轴居中）
      mainAxisAlignment: MainAxisAlignment.center,
      //默认是最大充满、还是根据child显示最小大小 这里设置为大小按照最小显示
      mainAxisSize: MainAxisSize.min,
      //水平方向 左对齐
      crossAxisAlignment: CrossAxisAlignment.start,
      // 添加三个 Text 垂直显示
      children: <Widget>[
        new Text("test1 "),
        new Text("test2 "),
        new Text("test3 "),
      ],
    );

    var row1 = new Row(
      //即是水平向居中（官方描述为 主轴居中）
      mainAxisAlignment: MainAxisAlignment.center,
      //大小按照最小显示
      mainAxisSize: MainAxisSize.min,
      //竖直方向 左对齐
      crossAxisAlignment: CrossAxisAlignment.start,
      // 添加三个 Text 水平
      children: <Widget>[
        new Text("test1 "),
        new Text("test2 "),
        new Text("test3 "),
      ],
    );

//    return Stack(
//      children: <Widget>[
//        buildTestForm()
//      ],
//    );
    return Stack(
      children: <Widget>[
        Container(
          color: Colors.white,
          alignment: Alignment.center,
          padding: EdgeInsets.only(left:30.0,right: 30.0),
          child: new Container(
            margin: EdgeInsets.only(top: 100),
            child: buildForm(),
          ),
        ),
      ],
    );
    //return getCardItem(Icons.star, "1000");
//    return Column(
//      children: <Widget>[
//        Container(
//          color: Colors.white,
//          child: Column(
//            children: <Widget>[
//              buildRowRadio(),
//              Padding(
//                padding: EdgeInsets.all(10),
//              ),
//              buildRowTitleRadio(),
//              Padding(
//                padding: EdgeInsets.all(10),
//              ),
//              buildRowFixTitleRadio(),
//              Padding(
//                padding: EdgeInsets.all(10),
//              ),
//              buildRadio(),
//            ],
//          ),
//        )
//      ],
//    );
  }

  var _tabIndex = 0;
  List<String> pageTitleList = ["首页", "我的"];
  List<Widget> pageList = new List();
  List<Widget> bottomBarList = new List();

  void initTabData() {
    for (int i = 0; i < pageTitleList.length; i++) {
      pageList.add(new EachView(pageTitleList[i]));
      bottomBarList.add(new EachView(pageTitleList[i]));
    }
  }

  Widget buildBottomTabScaffold() {
    //数组索引，通过改变索引值改变视图
    return new Scaffold(
      floatingActionButton: FloatingActionButton(
          onPressed: () {},
          child: Icon(
            Icons.add,
            color: Colors.white,
          )),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      backgroundColor: Colors.white,
      body: new IndexedStack(
        children: bottomBarList,
        index: _tabIndex,
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        shape: CircularNotchedRectangle(),
        child: Row(
          children: [
            IconButton(
              icon: Icon(Icons.home),
              onPressed: () {
                setState(() {
                  _tabIndex = 0;
                });
              },
            ),
            SizedBox(), //中间位置空出
            IconButton(
              icon: Icon(Icons.people),
              onPressed: () {
                setState(() {
                  _tabIndex = 1;
                });
              },
            ),
          ],
          mainAxisAlignment: MainAxisAlignment.spaceAround, //均分底部导航栏横向空间
        ),
      ),
      //drawer: new MyDrawer(),
    );
  }

  //通过“bottom”属性来添加一个导航栏底部tab按钮组，将要实现的效果如下：
  Widget buildTabScaffold() {
    List tabs = ["首页", "发现", "我的", "设置"];
    //用于控制/监听Tab菜单切换
    //TabBar和TabBarView正是通过同一个controller来实现菜单切换和滑动状态同步的。
    TabController tabController =
        TabController(length: tabs.length, vsync: this);
    return Scaffold(
        appBar: AppBar(
          title: Text('标题'),
          bottom: TabBar(
              //生成Tab菜单
              controller: tabController,
              tabs: tabs.map((e) => Tab(text: e)).toList()),
          //设置标题居中
          centerTitle: true,
          //设置最左侧按钮
          leading: Builder(builder: (context) {
            return IconButton(
              icon: Icon(Icons.bug_report, color: Colors.white),
              onPressed: () {
                // 打开抽屉菜单
                Scaffold.of(context).openDrawer();
              },
            );
          }),
          actions: <Widget>[
            //导航栏右侧菜单
            IconButton(icon: Icon(Icons.search), onPressed: () {}),
          ],
        ),
        backgroundColor: Colors.white,
        body: TabBarView(
          controller: tabController,
          //创建Tab页
          children: tabs.map((e) {
            return Container(
              alignment: Alignment.center,
              child: Text(e, textScaleFactor: 1),
            );
          }).toList(),
        ),
        //设置底部导航，通过Material组件库提供的BottomNavigationBar和BottomNavigationBarItem两个Widget来实现Material风格的底部导航栏
        bottomNavigationBar: BottomAppBar(
          color: Colors.white,
          shape: CircularNotchedRectangle(), // 底部导航栏打一个圆形的洞
          child: Row(
            children: [
              IconButton(icon: Icon(Icons.home)),
              SizedBox(), //中间位置空出
              IconButton(icon: Icon(Icons.business)),
            ],
            mainAxisAlignment: MainAxisAlignment.spaceAround, //均分底部导航栏横向空间
          ),
        ));
  }

  Widget buildDefaultScaffold() {
    return Scaffold(
        appBar: AppBar(
          title: Text('标题'),
          //设置标题居中
          centerTitle: true,
          //设置最左侧按钮
          leading: Builder(builder: (context) {
            return IconButton(
              icon: Icon(Icons.bug_report, color: Colors.white),
              onPressed: () {
                // 打开抽屉菜单
                Scaffold.of(context).openDrawer();
              },
            );
          }),
          actions: <Widget>[
            //导航栏右侧菜单
            IconButton(icon: Icon(Icons.search), onPressed: () {}),
          ],
        ),
        backgroundColor: Colors.white,
        //页面显示主体
        body: Container());
  }

  Widget buildFlexLayout() {
    return Column(
      children: <Widget>[
        //Flex的两个子widget按1：2来占据水平空间
        Flex(
          //子Widget的排列方式
          direction: Axis.horizontal,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(
                height: 30.0,
                color: Colors.red,
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                height: 30.0,
                color: Colors.blue,
              ),
            ),
          ],
        ),
      ],
    );
  }

  Widget buildCoulm() {
    // 构造 Android Java 语言中 LinearLayout orientation="vertical"  中的垂直布局
    //Expanded 在 Column 和 Row 中代表着平均充满，当有两个以上的widget存在的时候默认均分充满。同时页可以设置 flex 属性决定比例
    return Column(
      //表示水平方向子widget的布局顺序(是从左往右还是从右往左)，默认为系统当前Locale环境的文本方向(如中文、英语都是从左往右，而阿拉伯语是从右往左)
      textDirection: TextDirection.ltr,
      //即是竖直向居中（官方描述为 主轴居中）
      /**
       * 表示子Widgets在Column所占用的竖直空间内对齐方式，
       * 如果mainAxisSize值为MainAxisSize.min，则此属性无意义，因为子widgets的高度等于Row的高度。
       * 只有当mainAxisSize的值为MainAxisSize.max时，此属性才有意义，
       *    MainAxisAlignment.start表示沿textDirection的初始方向对齐，
       *    如textDirection取值为TextDirection.ltr时，则MainAxisAlignment.start表示左对齐，textDirection取值为TextDirection.rtl时表示从右对齐。
       *    而MainAxisAlignment.end和MainAxisAlignment.start正好相反；MainAxisAlignment.center表示居中对齐。
       *
       */
      mainAxisAlignment: MainAxisAlignment.center,
      //默认是最大充满、还是根据child显示最小大小 这里设置为大小按照最小显示
      //表示Column在主轴(竖直)方向占用的空间，默认是MainAxisSize.max，表示尽可能多的占用竖直方向的空间，
      // 此时无论子widgets实际占用多少水平空间，Column的宽度始终等于竖直方向的最大高度；
      //而MainAxisSize.min表示尽可能少的占用竖直空间，当子widgets没有占满水平剩余空间，则Row的实际宽度等于所有子widgets占用的的竖直空间
      mainAxisSize: MainAxisSize.min,

      //表示Row纵轴（垂直）的对齐方向，默认是VerticalDirection.down，表示从上到下
      verticalDirection: VerticalDirection.down,

      //水平方向 左对齐
      crossAxisAlignment: CrossAxisAlignment.start,
      // 添加三个 Text 垂直显示
      children: <Widget>[
        new Text("test1 "),
        new Text("test2 "),
        new Text("test3 "),
      ],
    );
  }

  TextEditingController unameController = new TextEditingController();
  TextEditingController pwdController = new TextEditingController();
  GlobalKey formKey = new GlobalKey<FormState>();

  Widget buildForm() {
    return Form(
      //设置globalKey，用于后面获取FormState
      key: formKey,
      //开启自动校验
      autovalidate: true,
      child: Column(
        children: <Widget>[
          TextFormField(
              autofocus: false,
              keyboardType: TextInputType.number,
              //键盘回车键的样式
              textInputAction: TextInputAction.next,
              controller: unameController,
              decoration: InputDecoration(
                  labelText: "用户名或邮箱",
                  hintText: "用户名或邮箱",
                  icon: Icon(Icons.person)),
              // 校验用户名
              validator: (v) {
                return v.trim().length > 0 ? null : "用户名不能为空";
              }),
          TextFormField(
              autofocus: false,
              controller: pwdController,
              decoration: InputDecoration(
                  labelText: "密码", hintText: "您的登录密码", icon: Icon(Icons.lock)),
              obscureText: true,
              //校验密码
              validator: (v) {
                return v.trim().length > 5 ? null : "密码不能少于6位";
              }),
          // 登录按钮
          Padding(
            padding: const EdgeInsets.only(top: 28.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: RaisedButton(
                    padding: EdgeInsets.all(15.0),
                    child: Text("登录"),
                    color: Theme.of(context).primaryColor,
                    textColor: Colors.white,
                    onPressed: () {
                      //在这里不能通过此方式获取FormState，context不对
                      //print(Form.of(context));
                      // 通过_formKey.currentState 获取FormState后，
                      // 调用validate()方法校验用户名密码是否合法，校验
                      // 通过后再提交数据。
                      if ((formKey.currentState as FormState).validate()) {
                        //验证通过提交数据
                      }
                    },
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget buildRowFixTitleRadio() {
    return Row(
      children: <Widget>[
        Flexible(
          child: RadioListTile<String>(
            value: '语文',
            title: Text('语文'),
            groupValue: _newValue,
            onChanged: (value) {
              setState(() {
                _newValue = value;
              });
            },
          ),
        ),
        Flexible(
          child: RadioListTile<String>(
            value: '数学',
            title: Text('数学'),
            groupValue: _newValue,
            onChanged: (value) {
              setState(() {
                _newValue = value;
              });
            },
          ),
        ),
        Flexible(
          child: RadioListTile<String>(
            value: '英语',
            title: Text('英语'),
            groupValue: _newValue,
            onChanged: (value) {
              setState(() {
                _newValue = value;
              });
            },
          ),
        ),
      ],
    );
  }

  Widget buildRowTitleRadio() {
    return Row(
      children: <Widget>[
        RadioListTile<String>(
          value: '语文',
          title: Text('语文'),
          groupValue: _newValue,
          onChanged: (value) {
            setState(() {
              _newValue = value;
            });
          },
        ),
        RadioListTile<String>(
          value: '数学',
          title: Text('数学'),
          groupValue: _newValue,
          onChanged: (value) {
            setState(() {
              _newValue = value;
            });
          },
        ),
        RadioListTile<String>(
          value: '英语',
          title: Text('英语'),
          groupValue: _newValue,
          onChanged: (value) {
            setState(() {
              _newValue = value;
            });
          },
        ),
      ],
    );
  }

  Widget buildRowRadio() {
    return Row(
      children: <Widget>[
        Radio<String>(
            //当前Radio的值
            value: "语文",
            //当前Radio 所在组的值
            //只有value 与groupValue 值一至时才会被选中
            groupValue: _newValue,
            onChanged: (value) {
              setState(() {
                _newValue = value;
              });
            }),
        Radio<String>(
            value: "数学",
            groupValue: _newValue,
            onChanged: (value) {
              setState(() {
                _newValue = value;
              });
            }),
        Radio<String>(
            value: "英语",
            groupValue: _newValue,
            onChanged: (value) {
              setState(() {
                _newValue = value;
              });
            }),
      ],
    );
  }

  String _newValue = '语文';

  //value 、groupValue 一起控制是否为选中状态，当groupValue = value时代表选中状态
  Widget buildRadio() {
    return Column(
      children: <Widget>[
        RadioListTile<String>(
          value: '语文',
          title: Text('语文'),
          groupValue: _newValue,
          onChanged: (value) {
            setState(() {
              _newValue = value;
            });
          },
        ),
        RadioListTile<String>(
          value: '数学',
          title: Text('数学'),
          groupValue: _newValue,
          onChanged: (value) {
            setState(() {
              _newValue = value;
            });
          },
        ),
        RadioListTile<String>(
          value: '英语',
          title: Text('英语'),
          groupValue: _newValue,
          onChanged: (value) {
            setState(() {
              _newValue = value;
            });
          },
        ),
      ],
    );
  }

  /**
   * activeColor → Color - 激活时原点的颜色。
      activeThumbImage → ImageProvider - 原点还支持图片，激活时的效果。
      activeTrackColor → Color - 激活时横条的颜色。
      inactiveThumbColor → Color - 非激活时原点的颜色。
      inactiveThumbImage → ImageProvider - 非激活原点的图片效果。
      inactiveTrackColor → Color - 非激活时横条的颜色。
      onChanged → ValueChanged - 改变时触发。
      value → bool - 切换按钮的值。
   */
  Widget buildSwich() {
    //维护单选开关状态
    bool _switchSelected = false;
    //维护复选框状态
    bool _checkboxSelected = true;
    return Column(
      children: <Widget>[
        Switch(
          //当前状态
          value: _switchSelected,
          // 激活时原点颜色
          activeColor: Colors.blue,
          inactiveTrackColor: Colors.blue.shade50,
          onChanged: (value) {
            //重新构建页面
            setState(() {
              _switchSelected = value;
            });
          },
        ),
        CupertinoSwitch(
          value: _switchSelected,
          onChanged: (value) {},
        ),
        new SwitchListTile(
          secondary: const Icon(Icons.shutter_speed),
          title: const Text('硬件加速'),
          value: _switchSelected,
          onChanged: (bool value) {
            setState(() {
              _switchSelected = !_switchSelected;
            });
          },
        ),
        Checkbox(
          //如果为 true，那么复选框的值可以是 true，false 或 null。
          tristate: true,
          value: _checkboxSelected,
          //选中时的颜色
          activeColor: Colors.red,
          // 改变时触发。
          onChanged: (value) {},
        ),
        new CheckboxListTile(
          secondary: const Icon(Icons.shutter_speed),
          title: const Text('硬件加速'),
          value: _checkboxSelected,
          onChanged: (bool value) {
            setState(() {
              _checkboxSelected = !_checkboxSelected;
            });
          },
        ),
      ],
    );
  }

  Widget buildButtonList() {
    return Column(
      children: <Widget>[
        buildRaisedButton(),
        Padding(
          padding: EdgeInsets.all(10),
        ),
        buildFlatButton(),
        Padding(
          padding: EdgeInsets.all(10),
        ),
        buildOutlineButton(),
        Padding(
          padding: EdgeInsets.all(10),
        ),
        buildIconButton(),
        Padding(
          padding: EdgeInsets.all(10),
        ),
        buildCustomButton(),
      ],
    );
  }

  Widget buildRaisedButton() {
    //它默认带有阴影和灰色背景。按下后，阴影会变大
    return RaisedButton(
      child: Text("RaisedButto"),
      onPressed: () => {},
    );
  }

  //
  Widget buildFlatButton() {
    //FlatButton即扁平按钮，默认背景透明并不带阴影。按下后，会有背景色：
    return FlatButton(
      child: Text("登录"),
      onPressed: () => {},
    );
  }

  Widget buildOutlineButton() {
    //OutlineButton默认有一个边框，不带阴影且背景透明。按下后，边框颜色会变亮、同时出现背景和阴影(较弱)：
    return OutlineButton(
      child: Text("登录"),
      onPressed: () => {},
    );
  }

  Widget buildIconButton() {
    return IconButton(
      icon: Icon(Icons.thumb_up),
      onPressed: () => {},
    );
  }

  Widget buildCustomButton() {
    return FlatButton(
      //按钮文字颜色
      textColor: Colors.white,
      //按钮禁用时的背景颜色
      disabledColor: Colors.grey,
      //按钮禁用时的文字颜色
      disabledTextColor: Colors.grey,
      //正常状态下的背景颜色
      color: Colors.blue,
      ////按钮按下时的背景颜色
      highlightColor: Colors.blue[700],
      ////按钮主题，默认是浅色主题
      colorBrightness: Brightness.dark,
      ////外形
      splashColor: Colors.grey,
      child: Text("Submit"),
      //圆角边框
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      //按钮点击回调
      onPressed: () => {},
    );
  }

  Widget buildDefaultTextField() {
    return TextField();
  }

  Widget buildTextField() {
    // 文本控制器
    final controller = TextEditingController();
    controller.addListener(() {
      print('input ${controller.text}');
    });

    //controller.text 使用这个方法来获取输入框的文字

    /**
     *  调用此方法 使用TextField 获取焦点
     *  FocusScope.of(context).requestFocus(nodeTwo);
     */
    FocusNode nodeOne = FocusNode();
    return Theme(
      data: new ThemeData(
          primaryColor: Colors.blue.shade300, hintColor: Colors.blue),
      child: TextField(
        controller: controller,
        //是否隐藏密码
        obscureText: false,
        //绑定焦点控制
        focusNode: nodeOne,
        /**
         * TextCapitalization.sentences  这是最常见的大写化类型，每个句子的第一个字母被转换成大写。
         * TextCapitalization.characters  大写句子中的所有字符。
         * TextCapitalization.words 对每个单词首字母大写。
         */
        textCapitalization: TextCapitalization.sentences,
        //控制光标样式
        cursorColor: Colors.red,
        cursorRadius: Radius.circular(6.0),
        cursorWidth: 6.0,
        //自动获取焦点
        autofocus: false,
        //最大长度，设置此项会让TextField右下角有一个输入数量的统计字符串
        maxLength: 30,
        //输入文本的样式
        style: TextStyle(fontSize: 14.0, color: Colors.red),
        //在弹出键盘的时候修改键盘类型
        keyboardType: TextInputType.number,
        //键盘回车键的样式
        textInputAction: TextInputAction.send,
        //允许的输入格式 WhitelistingTextInputFormatter.digitsOnly 只允许输入数字
        inputFormatters: [WhitelistingTextInputFormatter.digitsOnly],
        //内容改变的回调
        onChanged: (text) {
          print('change $text');
        },
        //内容提交(按回车)的回调
        onSubmitted: (text) {
          print('submit $text');
        },
        //键盘上按了done
        onEditingComplete: () {},
        decoration: InputDecoration(
            hintText: "请输入文字",
            hintStyle:
                TextStyle(fontWeight: FontWeight.w300, color: Colors.red),
            //填充颜色
            fillColor: Colors.blue.shade50,
            filled: true,
            // 边框的内边距
            contentPadding: EdgeInsets.all(10.0),
            //边框
            border: OutlineInputBorder(
              //边框圆角
              borderRadius: BorderRadius.circular(5.0),
            )),
      ),
    );
  }

  getDefaultTextStyle() {
    return TextStyle(
      //文字的颜色
      color: Colors.blue,
      //该属性和Text的textScaleFactor都用于控制字体大小
      //fontSize可以精确指定字体大小，而textScaleFactor只能通过缩放比例来控制。
      fontSize: 18.0,
      //该属性用于指定行高，但它并不是一个绝对值，而是一个因子，具体的行高等于fontSize*height
      height: 1.2,
      fontFamily: "Courier",
      background: new Paint()..color = Colors.yellow,
      //线的颜色
      decorationColor: const Color(0xffffffff),
      //none无文字装饰   lineThrough删除线   overline文字上面显示线    underline文字下面显示线
      decoration: TextDecoration.underline,
      //文字装饰的风格  dashed,dotted虚线(简短间隔大小区分)  double三条线  solid两条线
      decorationStyle: TextDecorationStyle.solid,
      //单词间隙(负值可以让单词更紧凑)
      wordSpacing: 0.0,
      //字母间隙(负值可以让字母更紧凑)
      letterSpacing: 0.0,
      //文字样式，斜体和正常
      fontStyle: FontStyle.italic,
      //字体粗细  粗体和正常
      fontWeight: FontWeight.w900,
    );
  }

  getRichTextWidget() {
    var textRich = new Text.rich(
      new TextSpan(
        text: 'Hello world',
        style: new TextStyle(
          color: Colors.white,
          fontSize: 14.0,
          decoration: TextDecoration.none,
        ),
        children: <TextSpan>[
          new TextSpan(
            text: '拼接1',
          ),
          new TextSpan(
            text: '拼接7',
            style: new TextStyle(
              color: Colors.green,
            ),
            recognizer: new TapGestureRecognizer()
              ..onTap = () {
                //增加一个点击事件
                print('1');
              },
          ),
        ],
      ),
    );
    return textRich;
  }

  getTextWidget() {
    //构造方法创建，只能生成一种style
    var textWidget = Text(
      "Hello world",
      //文本的对齐方式；可以选择左对齐、右对齐还是居中。
      textAlign: TextAlign.center,
      //文本方向
      textDirection: TextDirection.ltr,
      //是否自动换行 false文字不考虑容器大小  单行显示   超出；屏幕部分将默认截断处理
      softWrap: false,
      //指定文本显示的最大行数，默认情况下，文本是自动折行的
      maxLines: 1,
      //如果有多余的文本，可以通过overflow来指定截断方式，默认是直接截断，
      //TextOverflow.clip剪裁   TextOverflow.fade 渐隐  TextOverflow.ellipsis省略号
      overflow: TextOverflow.ellipsis,
      //代表文本相对于当前字体大小的缩放因子，相对于去设置文本的样式style属性
      textScaleFactor: 1.5,
      style: TextStyle(
        //文字的颜色
        color: Colors.blue,
        //该属性和Text的textScaleFactor都用于控制字体大小
        //fontSize可以精确指定字体大小，而textScaleFactor只能通过缩放比例来控制。
        fontSize: 18.0,
        //该属性用于指定行高，但它并不是一个绝对值，而是一个因子，具体的行高等于fontSize*height
        height: 1.2,
        fontFamily: "Courier",
        background: new Paint()..color = Colors.yellow,
        //线的颜色
        decorationColor: const Color(0xffffffff),
        //none无文字装饰   lineThrough删除线   overline文字上面显示线    underline文字下面显示线
        decoration: TextDecoration.underline,
        //文字装饰的风格  dashed,dotted虚线(简短间隔大小区分)  double三条线  solid两条线
        decorationStyle: TextDecorationStyle.solid,
        //单词间隙(负值可以让单词更紧凑)
        wordSpacing: 0.0,
        //字母间隙(负值可以让字母更紧凑)
        letterSpacing: 0.0,
        //文字样式，斜体和正常
        fontStyle: FontStyle.italic,
        //字体粗细  粗体和正常
        fontWeight: FontWeight.w900,
      ),
    );

    return textWidget;
  }

  getCardItem(IconData icon, String text) {
    return new Container(
        color: Colors.grey,
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new Padding(padding: EdgeInsets.only(top: 4)),
            new Card(
              child: new FlatButton(
                  onPressed: () => {print("flat button pressed")},
                  child: new Padding(
                    padding: new EdgeInsets.only(
                        left: 0, top: 0, right: 0, bottom: 10),
                    child: new Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        new Padding(padding: EdgeInsets.only(top: 10)),
                        new Container(
                            child: new Text(
                              "这是一点描述",
                              style: TextStyle(
                                color: Colors.green,
                                fontSize: 14.0,
                              ),

                              ///最长三行，超过 ... 显示
                              maxLines: 3,
                              overflow: TextOverflow.ellipsis,
                            ),
                            margin: new EdgeInsets.only(top: 0.0, bottom: 0.0),
                            alignment: Alignment.topLeft),
                        new Padding(padding: EdgeInsets.only(top: 4.0)),

                        ///三个平均分配的横向图标文字
                        new Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            getBottomItem(Icons.star, "1000"),
                            getBottomItem(Icons.link, "1000"),
                            getBottomItem(Icons.subject, "1000"),
                          ],
                        ),
                      ],
                    ),
                  )),
            ),
          ],
        ));
  }

  ///返回一个居中带图标和文本的Item
  getBottomItem(IconData icon, String text) {
    ///充满 Row 横向的布局
    return new Container(
      color: Colors.white,
      padding: EdgeInsets.only(right: 10),
      child: new Row(
        //主轴居中,即是横向居中
        mainAxisAlignment: MainAxisAlignment.center,
        //大小按照最大充满
        mainAxisSize: MainAxisSize.max,
        //竖向也居中
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          //一个图标，大小16.0，灰色
          new Icon(
            icon,
            size: 16.0,
            color: Colors.grey,
          ),
          //间隔
          new Padding(padding: new EdgeInsets.only(left: 6.0)),

          ///显示文本
          new Text(
            text,
            //设置字体样式：颜色灰色，字体大小14.0
            style: new TextStyle(color: Colors.grey, fontSize: 14.0),
            //超过的省略为...显示
            overflow: TextOverflow.ellipsis,
            //最长一行
            maxLines: 1,
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    ///销毁
    super.dispose();
  }
}

//子页面
//代码中设置了一个内部的_title变量，这个变量是从主页面传递过来的，然后根据传递过来的具体值显示在APP的标题栏和屏幕中间。
class EachView extends StatefulWidget {
  String _title;

  EachView(this._title);

  @override
  _EachViewState createState() => _EachViewState();
}

class _EachViewState extends State<EachView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(widget._title)),
      body: Center(child: Text(widget._title)),
    );
  }
}
