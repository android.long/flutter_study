import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_study_test/app/base/LogHelper.dart';
import 'package:flutter_study_test/app/page/home_item_main.dart';

/**
 * 仿开源中国 底部菜单栏
 */
class HomeTestPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => new MyOSCClientState();
}

class MyOSCClientState extends State<HomeTestPage> {
  //当前显示的页面index
  int _tabIndex = 0;

  //底部显示的tab image
  var tabImages;

  //tab页面
  var _body;

  //底部显示的tab
  var appBarTitles = ['资讯', '动弹', '发现', '我的'];

  //底部tab 文字默认显示颜色
  final tabTextStyleNormal = new TextStyle(color: const Color(0xff969696));

  //底部tab 文字选中显示颜色
  final tabTextStyleSelected = new TextStyle(color: const Color(0xff63ca6c));

  List<BottomNavigationBarItem> getBarItems() {
    List<BottomNavigationBarItem> items = new List();
    for (int i = 0; i < appBarTitles.length; i++) {
      items.add(new BottomNavigationBarItem(
          icon: getTabIcon(i), title: getTabTitle(i)));
    }
    return items;
  }

  void initData() {
    if (tabImages == null) {
      tabImages = [
        [
          getTabImage('images/ic_nav_news_normal.png'),
          getTabImage('images/ic_nav_news_actived.png')
        ],
        [
          getTabImage('images/ic_nav_tweet_normal.png'),
          getTabImage('images/ic_nav_tweet_actived.png')
        ],
        [
          getTabImage('images/ic_nav_discover_normal.png'),
          getTabImage('images/ic_nav_discover_actived.png')
        ],
        [
          getTabImage('images/ic_nav_my_normal.png'),
          getTabImage('images/ic_nav_my_pressed.png')
        ]
      ];
    }
    _body = new IndexedStack(
      children: <Widget>[
        new Container(
          color: Colors.yellow,
        ),
        new Container(
          color: Colors.red,
        ),
        new Container(
          color: Colors.grey,
        ),
        new Container(
          color: Colors.blue,
        )
      ],
      index: _tabIndex,
    );
  }

  TextStyle getTabTextStyle(int curIndex) {
    if (curIndex == _tabIndex) {
      return tabTextStyleSelected;
    }
    return tabTextStyleNormal;
  }

  Image getTabImage(path) {
    return new Image.asset(path, width: 20.0, height: 20.0);
  }

  Image getTabIcon(int curIndex) {
    if (curIndex == _tabIndex) {
      return tabImages[curIndex][1];
    }
    return tabImages[curIndex][0];
  }

  Text getTabTitle(int curIndex) {
    return new Text(appBarTitles[curIndex], style: getTabTextStyle(curIndex));
  }

  @override
  Widget build(BuildContext context) {
    initData();
    return new MaterialApp(
      home: new Scaffold(
        backgroundColor: Colors.white,
        body: _body,
        bottomNavigationBar: BottomAppBar(
          color: Colors.white,
          shape: CircularNotchedRectangle(),
          child: new CupertinoTabBar(
            items: getBarItems(),
            currentIndex: _tabIndex,
            onTap: (index) {
              setState(() {
                _tabIndex = index;
                LogHelper.info("ontap " + index.toString());
              });
            },
          ),
        ),
        //drawer: new MyDrawer(),
      ),
    );
  }
}
