import 'dart:async';

import 'package:base_library/base_library.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_study_test/app/page/splash_page.dart';
import 'package:flutter_study_test/test/dialog/message_dialog.dart';
import 'package:flutter_study_test/test/native/InteractNative.dart';

import 'AndroidCodePage.dart';
import 'AppBarPage.dart';
import 'DemoCroBar.dart';
import 'DemoDefaultGridView.dart';
import 'DemoImageContainer.dart';
import 'DemoImagePage.dart';
import 'DemoListView.dart';
import 'DemoNoScroBar.dart';
import 'DemoRefreshListView.dart';
import 'DemoStateFulWidget.dart';
import 'DemoStateFulWidget2.dart';
import 'DemoTabBarAndTopTab.dart';
import 'DemoHomeTestPage.dart';
import 'DemoTabBarTab.dart';
import 'DetaileCoulmnAndRow.dart';
import 'DialogPage.dart';
import 'JsonSeralizablePage.dart';
import 'ScanCodePage.dart';
import 'SwiperPage.dart';
import 'ToastPage.dart';

class IndexPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new IndexPageState();
  }
}

class IndexPageState extends State<IndexPage> {
  //声明一个调用对象，需要把kotlin中注册的ChannelName传入构造函数
  static const _platform =
      const MethodChannel('com.mrper.framework.plugins/toast');
  StreamSubscription _subscription ;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (null == _subscription) {
      _subscription = InteractNative.dealNativeWithValue()
          .listen(_onEvent, onError: _onError);
    }
  }

  void _onEvent(Object event) {
    if ('onConnected' == event) {
    }

    Navigator.push(
        context, new CupertinoPageRoute(builder: (context) => ImageShowPage(event)));
    print("event "+event);
  }

  void _onError(Object error) {

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        //在标题前面显示的一个控件，在首页通常显示应用的 logo；在其他界面通常显示为返回按钮
        leading: IconButton(icon: Icon(Icons.home), onPressed: () {print("back");}),
        //Toolbar 中主要内容，通常显示为当前界面的标题文字
        title: Text("测试列表"),
      ),
      //显示的页面
      body: buildPage(),
    );
  }

  pushWidgeet(Widget widget) {
    Navigator.push(
        context, new CupertinoPageRoute(builder: (context) => widget));
  }

  Widget buildPage() {
    var column = new Column(
      children: <Widget>[
        RaisedButton(
          child: Text("AppBar页面"),
          onPressed: () {
            pushWidgeet(new AppBarPage());
          },
        ),
        RaisedButton(
          child: Text("底部菜单和中间凸起按钮效果"),
          onPressed: () {
            pushWidgeet(new DemoStateFulWidget2());
          },
        ),
        RaisedButton(
          child: Text("仿开源中国底部菜单"),
          onPressed: () {
            pushWidgeet(HomeTestPage());
          },
        ),
        RaisedButton(
          child: Text("TabBar下面的Tab菜单"),
          onPressed: () {
            pushWidgeet(TabBarAndTopTab());
          },
        ),
        RaisedButton(
          child: Text("TabBar中的Tab菜单"),
          onPressed: () {
            pushWidgeet(new TabBarTabPage());
          },
        ),
        RaisedButton(
          child: Text("动态list Scroll 有scrollbar"),
          onPressed: () {
            pushWidgeet(new DemoScroBar());
          },
        ),
        RaisedButton(
          child: Text("动态list Scroll 无scrollbar"),
          onPressed: () {
            pushWidgeet(new DemoNoScroBar());
          },
        ),
        RaisedButton(
          child: Text(" ListView "),
          onPressed: () {
            pushWidgeet(new DemoListView());
          },
        ),
        RaisedButton(
          child: Text(" ListView 下拉刷新  "),
          onPressed: () {
            pushWidgeet(new DemoRefreshListView());
          },
        ),
        RaisedButton(
          child: Text(" GridView "),
          onPressed: () {
            pushWidgeet(new DemoDefaultGridView());
          },
        ),
        RaisedButton(
          child: Text(" 轮播图 "),
          onPressed: () {
            pushWidgeet(new SwiperPage());
          },
        ),
        RaisedButton(
            child: Text(" 轮播图 整页分页 "),
            onPressed: () {
              pushWidgeet(new SplashPage());
            }),
        RaisedButton(
            child: Text(" Toast "),
            onPressed: () {
              pushWidgeet(new ToastPage());
            }),
        RaisedButton(
            child: Text(" 网络请求 "),
            onPressed: () {
              pushWidgeet(new JsonSeralizablePage());
            }),
        RaisedButton(
            child: Text(" dialog "),
            onPressed: () {
              pushWidgeet(new DialogPage());
            }),
        RaisedButton(
            child: Text(" 调用android原生 方法 并测试回调 "),
            onPressed: () {
              openAndroidLibTestFunction();
            }),
        RaisedButton(
            child: Text(" 调用android原生 toast "),
            onPressed: () {
              openAndroidLibToastFunction();
            }),
        RaisedButton(
            child: Text(" 调用android原生 自定义相机 "),
            onPressed: () {
              openAndroidLibCameraFunction();
            }),
        RaisedButton(
            child: Text(" 扫一扫 "),
            onPressed: () {
              pushWidgeet(new ScanCodePage());
            }),
        RaisedButton(
            child: Text(" Android 扫一扫 "),
            onPressed: () {
              pushWidgeet(new AndroidCodePage());
            }),
        RaisedButton(
            child: Text(" 精通 Cloumn Row "),
            onPressed: () {
              pushWidgeet(new ColumnRowPage());
            }),
        RaisedButton(
            child: Text(" Image "),
            onPressed: () {
              pushWidgeet(new ImageElement());
            }),


      ],
    );
    return Scrollbar(
      child: SingleChildScrollView(
        // 设置滚动的方向, 默认垂直方向
        scrollDirection: Axis.vertical,
        // 设置显示方式 reverse: false，垂直方向,则滚动内容头部和左侧对其, 那么滑动方向就是从左向右
        reverse: false,
        padding: EdgeInsets.all(0.0),
        physics: BouncingScrollPhysics(),
        child: new Container(
          child: Center(
            child: column,
          ),
        ),
      ),
    );
  }

  void openAndroidLibToastFunction() {
    LogUtil.e("open android lib");
    Map<String, String> map = {"username": "123", "password": "456"};
    InteractNative.showToast("测试toast");
  }

  void openAndroidLibTestFunction() {
    //参数传递
    Map<String, String> map = {"username": "123", "password": "1456"};
    InteractNative.goNativeWithValue(
            InteractNative.methodNames['register'], map)
        .then((success) {
      //android 原生方法的回调
      if (success == true) {
        print('注册成功');
        Navigator.pop(context);
      } else if (success is String) {
        print(success);
      } else {
        print('注册失败');
      }
    });
  }

  void openAndroidLibCameraFunction() {
    InteractNative.openCamer();
  }
}
