import 'dart:async';

import 'package:base_library/base_library.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_study_test/app/page/splash_page.dart';
import 'package:flutter_study_test/test/base/BaseWidget.dart';
import 'package:flutter_study_test/test/dialog/message_dialog.dart';
import 'package:flutter_study_test/test/native/InteractNative.dart';

import 'DemoCroBar.dart';
import 'DemoDefaultGridView.dart';
import 'DemoImageContainer.dart';
import 'DemoListView.dart';
import 'DemoNoScroBar.dart';
import 'DemoRefreshListView.dart';
import 'DemoStateFulWidget.dart';
import 'DemoStateFulWidget2.dart';
import 'DemoTabBarAndTopTab.dart';
import 'DemoHomeTestPage.dart';
import 'DemoTabBarTab.dart';
import 'DialogPage.dart';
import 'JsonSeralizablePage.dart';
import 'SwiperPage.dart';
import 'ToastPage.dart';

class AppBarPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new AppBarPageState();
  }
}

class AppBarPageState extends BasePageState<AppBarPage> {
  Widget buildView() {
    return Container(
        child: Column(
      children: <Widget>[
        RaisedButton(
            child: Text("无返回按钮 "),
            onPressed: () {
              buildDefaultBar("标题");
              setState(() {});
            }),
        RaisedButton(
            child: Text("显示返回按钮 "),
            onPressed: () {
              buildBackBar("标题");
              setState(() {});
            }),
        RaisedButton(
            child: Text("显示返回 和pop "),
            onPressed: () {
              buildBackAndOtherBar("测试2", actions: <Widget>[
                IconButton(icon: Icon(Icons.share), onPressed: () {}),
                PopupMenuButton(
                  itemBuilder: (BuildContext context) =>
                      <PopupMenuItem<String>>[
                    PopupMenuItem<String>(
                      child: Text("热度"),
                      value: "hot",
                    ),
                    PopupMenuItem<String>(
                      child: Text("最新"),
                      value: "new",
                    ),
                  ],
                  onSelected: (String action) {
                    switch (action) {
                      case "hot":
                        print("hot");
                        break;
                      case "new":
                        print("new");
                        break;
                    }
                  },
                  onCanceled: () {
                    print("onCanceled");
                  },
                )
              ]);
              setState(() {});
            }),
        RaisedButton(
            child: Text("显示返回 和分享 "),
            onPressed: () {
              buildBackAndOtherBar("测试3", actions: <Widget>[
                IconButton(icon: Icon(Icons.share), onPressed: () {}),
              ]);
              setState(() {});
            }),
        RaisedButton(child: Text("显示返回 和pop 位置修改 "), onPressed: () {}),
      ],
    ));
  }

  @override
  buildInitState() {}

  @override
  Widget buildWidget(BuildContext context) {
    return buildView();
  }
}
