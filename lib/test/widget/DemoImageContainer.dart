import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_study_test/test/dialog/message_dialog.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:fluttertoast/fluttertoast.dart';

class ImageShowPage extends StatefulWidget {
  String imageFile;

  ImageShowPage(this.imageFile);

  @override
  ImageShowPageState createState() {
    return ImageShowPageState(imageFile);
  }
}

class ImageShowPageState extends State<ImageShowPage> {

  String imageFile;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ToastPage'),
      ),
      body: Container(
        child: Center(
          child: new Image.file(new File(imageFile)),
        ),
      ),
    );
  }

  ImageShowPageState(this.imageFile);
}
