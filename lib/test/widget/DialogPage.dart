import 'package:flutter/material.dart';
import 'package:flutter_study_test/test/dialog/message_dialog.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:fluttertoast/fluttertoast.dart';

class DialogPage extends StatefulWidget {
  @override
  DialogPageState createState() {
    return DialogPageState();
  }
}

class DialogPageState extends State<DialogPage> {
  void showDefaultDialog(BuildContext context, String message) {
    if (message != null && message.isNotEmpty) {
      showDialog<Null>(
          context: context, //BuildContext对象
          barrierDismissible: false,
          builder: (BuildContext context) {
            return new MessageDialog(title: "提示", message: message);
            //调用对话框);
          });
    }
  }

  ///弹对话框
  void showToastDialog(BuildContext context, String message, {String title = "提示", String negativeText = "确定", Function callBack}) {
    if (message != null && message.isNotEmpty) {
      showDialog<Null>(
          context: context, //BuildContext对象
          barrierDismissible: false,
          builder: (BuildContext context) {
            return new MessageDialog(
              title: title,
              negativeText: negativeText,
              message: message,
              onCloseEvent: () {
                Navigator.pop(context);
                if (callBack != null) {
                  callBack();
                }
              },
            );
            //调用对话框);
          });
    }
  }

  void showCanDialog(BuildContext context, String message, {String title = "提示", String negativeText = "确定", String positiveText = "取消", Function callBack, Function canCallBack}) {
    if (message != null && message.isNotEmpty) {
      showDialog<Null>(
          context: context, //BuildContext对象
          barrierDismissible: false,
          builder: (BuildContext context) {
            return new MessageDialog(
              title: title,
              negativeText: negativeText,
              positiveText: positiveText,
              message: message,
              onPositivePressEvent: () {
                Navigator.pop(context);
                if (canCallBack != null) {
                  canCallBack();
                }
              },
              onCloseEvent: () {
                Navigator.pop(context);
                if (callBack != null) {
                  callBack();
                }
              },
            );
            //调用对话框);
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('ToastPage'),
      ),
      body: Column(children: <Widget>[
        RaisedButton(
            child: Text(" 无按钮 "),
            onPressed: () {
              showDefaultDialog(context, "err");
            }),
        RaisedButton(
            child: Text(" 只有确认提示 "),
            onPressed: () {
              showToastDialog(context, "err");
            }),
        RaisedButton(
            child: Text(" 确认取消提示 "),
            onPressed: () {
              showCanDialog(context, "err",
                  negativeText: "确认", positiveText: "取消", callBack: () {
                print("确认");
              }, canCallBack: () {
                print("取消");
              });
            })
      ]),
    );
  }
}
