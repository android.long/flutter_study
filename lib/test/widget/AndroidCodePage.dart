import 'dart:async';

import 'package:base_library/base_library.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_study_test/app/page/splash_page.dart';
import 'package:flutter_study_test/test/base/BaseWidget.dart';
import 'package:flutter_study_test/test/dialog/message_dialog.dart';
import 'package:flutter_study_test/test/native/InteractNative.dart';

import 'DemoCroBar.dart';
import 'DemoDefaultGridView.dart';
import 'DemoImageContainer.dart';
import 'DemoListView.dart';
import 'DemoNoScroBar.dart';
import 'DemoRefreshListView.dart';
import 'DemoStateFulWidget.dart';
import 'DemoStateFulWidget2.dart';
import 'DemoTabBarAndTopTab.dart';
import 'DemoHomeTestPage.dart';
import 'DemoTabBarTab.dart';
import 'DialogPage.dart';
import 'JsonSeralizablePage.dart';
import 'SwiperPage.dart';
import 'ToastPage.dart';

class AndroidCodePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new AndroidCodePageState();
  }
}

class AndroidCodePageState extends BasePageState<AndroidCodePage> {
  Widget buildView() {
    return Container(
      child: new Center(
        child: new Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 15.0, vertical: 8.0),
              child: RaisedButton(
                  color: Colors.orange,
                  textColor: Colors.white,
                  splashColor: Colors.blueGrey,
                  onPressed: scan,
                  child: const Text('启动')),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
              child: Text(
                barcode,
                textAlign: TextAlign.center,
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  buildInitState() {
    buildBackBar("Android  扫一扫");
  }

  @override
  Widget buildWidget(BuildContext context) {
    return buildView();
  }

  String barcode = "";

  scan() {
    InteractNative.goNativeWithValue(
        InteractNative.methodNames['scan'])
        .then((success) {
      //android 原生方法的回调
      if (success == true) {
        print('注册成功');
        Navigator.pop(context);
      } else if (success is String) {
        print(success);
      } else {
        print('注册失败');
      }
    });
  }
}
