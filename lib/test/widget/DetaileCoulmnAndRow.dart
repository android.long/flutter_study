import 'dart:async';

import 'package:base_library/base_library.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_study_test/app/page/splash_page.dart';
import 'package:flutter_study_test/test/base/BaseWidget.dart';
import 'package:flutter_study_test/test/dialog/message_dialog.dart';
import 'package:flutter_study_test/test/native/InteractNative.dart';

import 'DemoCroBar.dart';
import 'DemoDefaultGridView.dart';
import 'DemoImageContainer.dart';
import 'DemoListView.dart';
import 'DemoNoScroBar.dart';
import 'DemoRefreshListView.dart';
import 'DemoStateFulWidget.dart';
import 'DemoStateFulWidget2.dart';
import 'DemoTabBarAndTopTab.dart';
import 'DemoHomeTestPage.dart';
import 'DemoTabBarTab.dart';
import 'DialogPage.dart';
import 'JsonSeralizablePage.dart';
import 'SwiperPage.dart';
import 'ToastPage.dart';

class ColumnRowPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new ColumnRowPageState();
  }
}

class ColumnRowPageState extends BasePageState<ColumnRowPage> {
  Widget buildView() {
    return Container(
      color: Colors.white,
      child: buildTwoRow(),
    );
  }

  Widget buildTwoRow() {
    return Row(
      //水平方向填充
      mainAxisSize: MainAxisSize.max,
      //平分空白
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(color: Colors.grey,height: 120,),
        ),
        Expanded(
          flex: 1,
          child: Container(color: Colors.green,height: 120,),
        ),
        Expanded(
          flex: 1,
          child: Container(color: Colors.blue,height: 120,),
        ),
      ],
    );
  }

  Widget buildFirstRow() {
    return Row(
      children: <Widget>[
        Text("a"),
        Text("b"),
        Text("c"),
      ],
    );
  }

  Widget buildFirstColumn() {
    return Column(
      children: <Widget>[
        Text("a"),
        Text("b"),
        Text("c"),
      ],
    );
  }

  @override
  buildInitState() {
    buildBackBar("水平垂直布局");
  }

  @override
  Widget buildWidget(BuildContext context) {
    return buildView();
  }
}
