import 'package:flutter/services.dart';

class InteractNative {
  /* 通道名称，必须与原生注册的一致*/
  static const flutter_to_native = const MethodChannel('com.flutter_to_native');

  /* 通道名称，必须与原生注册的一致*/
  static const native_to_flutter = const EventChannel('com.native_to_flutter');

  /*
   * 方法名称，必须与flutter注册的一致
   */
  static final Map<String, String> methodNames = const {
    'register': 'register',
    'scan': 'scan',
  };

  /*
  * 调用原生的方法（带参）
  */
  static Future<dynamic> goNativeWithValue(String methodName,
      [Map<String, String> map]) async {
    if (null == map) {
      dynamic future = await flutter_to_native.invokeMethod(methodName);
      return future;
    } else {
      dynamic future = await flutter_to_native.invokeMethod(methodName, map);
      return future;
    }
  }

  static Future<dynamic> showToast(String message) async {
    Map<String, String> map = {"message": message, "password": "456"};
    dynamic future = await flutter_to_native.invokeMethod("toast", map);
    return future;
  }

  /*
  * 原生回调的方法（带参）
  *
      StreamSubscription _subscription = InteractNative.dealNativeWithValue()
          .listen(_onEvent, onError: _onError);
            void _onEvent(Object event) {
    if ('onConnected' == event) {
//        DialogUtil.buildToast('已连接');
    }
  }

  void _onError(Object error) {
    DialogUtil.buildToast(error.toString());
  }
  */
  static Stream<dynamic> dealNativeWithValue() {
    Stream<dynamic> stream = native_to_flutter.receiveBroadcastStream();
    return stream;
  }

  static Future<dynamic> openCamer() async {
    dynamic future = await flutter_to_native.invokeMethod("camer");
    return future;
  }
}
