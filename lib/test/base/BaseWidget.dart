import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

abstract class BasePageState<T> extends State<StatefulWidget> {
  AppBar appBar;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
   buildInitState();

  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      appBar: appBar,
      //显示的页面
      body: buildWidget(context),
    );
  }

  Widget buildWidget(BuildContext context);
  buildInitState();


  Widget buildDefaultBar(String title) {
   return appBar =  AppBar(
      //标题居中显示
      centerTitle: true,
      //返回按钮占位
      leading: Container(),
      //标题显示
      title: Text(title),
    );
  }

  /**
   * title appBar 显示的标题文字
   * backIcon  appBar 显示的返回键图标
   */
  Widget buildBackBar(String title,{backIcon=Icons.arrow_back_ios}) {
    return appBar =AppBar(
      centerTitle: true,
      //在标题前面显示的一个控件，在首页通常显示应用的 logo；在其他界面通常显示为返回按钮
      leading: IconButton(
          icon: Icon(backIcon),
          onPressed: () {
            Navigator.pop(context);
          }),
      //Toolbar 中主要内容，通常显示为当前界面的标题文字
      title: Text(title),
    );
  }
  /**
   * title appBar 显示的标题文字
   * backIcon  appBar 显示的返回键图标
   * actions  appBar 最右侧的图标集合
   */
  Widget buildBackAndOtherBar(String title,{backIcon=Icons.arrow_back_ios,List<Widget> actions}) {
    return appBar =AppBar(
      centerTitle: true,
      //在标题前面显示的一个控件，在首页通常显示应用的 logo；在其他界面通常显示为返回按钮
      leading: IconButton(
          icon: Icon(backIcon),
          onPressed: () {
            Navigator.pop(context);
          }),
      //Toolbar 中主要内容，通常显示为当前界面的标题文字
      title: Text(title),
      //标题右侧显示的按钮组
      actions:actions,
    );
  }


}