import 'package:json_annotation/json_annotation.dart';

class UserBean {
  String  name ;
  String desc;

  String author;

  int publishTime;

  var envelopePic;

  UserBean(data) {
      this.name = data["courseTitle"];
      this.desc = data["courseFlag"];
      this.author = data["courseCreateUserName"];

      this.publishTime = int.parse(data["createTime"]);
      this.envelopePic = data["courseTitlePic"];
  }

}