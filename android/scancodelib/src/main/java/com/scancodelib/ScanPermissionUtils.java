package com.scancodelib;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.provider.Settings;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;


/**
 * Create by alv1 on 2019/6/12
 */
public class ScanPermissionUtils {

    public ScanPermissionUtils(Context context, ScanPermissionReCallback pAnalyzeCallback){
        mContext = context;
        mScanPermissionReCallback=pAnalyzeCallback;
        initPicSelectFunction();
        checkPermissing();
    }

    private ScanPermissionReCallback mScanPermissionReCallback;
    private Context mContext;
    private String[] permissions = new String[3];

    private void initPicSelectFunction() {
        permissions[0] = Manifest.permission.READ_EXTERNAL_STORAGE;
        permissions[1] = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        permissions[2] = Manifest.permission.CAMERA;
    }

    private boolean checkPermissions(String[] permissions) {
        for (String perm : permissions) {
            if (ActivityCompat.checkSelfPermission(mContext, perm) != PackageManager.PERMISSION_GRANTED) {
                return false;
            }
        }
        return true;
    }
    public void onRequestPermissionsResult(String[] permissions) {
        if (checkPermissions(permissions)) {
            mScanPermissionReCallback.onSuccess();
        } else {
            showReasonAndRequestAgainIfCouldbe();
        }
    }

    private void showReasonAndRequestAgainIfCouldbe() {
        if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext, Manifest.permission.CAMERA)) {
            showAlert("该功能需要您授予拍照权限,否则将无法使用", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions((Activity) mContext, new String[]{Manifest.permission.CAMERA}, 0);
                }
            });
        } else if (!ActivityCompat.shouldShowRequestPermissionRationale((Activity) mContext, Manifest.permission.CAMERA) && ActivityCompat.checkSelfPermission(((Activity) mContext), Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED) {//用户拒绝并勾选了“不再提示”框，跳转应用设置界面

            showAlert("该功能需要您授予拍照权限,请手动开启权限", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Uri packageURI = Uri.parse("package:" + ((Activity) mContext).getPackageName());
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, packageURI);
                    ((Activity) mContext).startActivity(intent);
                }
            });
        } else if (ActivityCompat.shouldShowRequestPermissionRationale(((Activity) mContext), Manifest.permission.READ_EXTERNAL_STORAGE)) {
            showAlert("该功能需要读取您的内存卡上的图片", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ActivityCompat.requestPermissions(((Activity) mContext), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
                }
            });

        } else if (!ActivityCompat.shouldShowRequestPermissionRationale(((Activity) mContext), Manifest.permission.READ_EXTERNAL_STORAGE) && ActivityCompat.checkSelfPermission(((Activity) mContext), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            //用户拒绝并勾选了“不再提示”框，跳转应用设置界面
            showAlert("该功能需要读取您的内存卡上的图片,请手动开启权限", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Uri packageURI = Uri.parse("package:" + ((Activity) mContext).getPackageName());
                    Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, packageURI);
                    ((Activity) mContext).startActivity(intent);
                }
            });
        }
    }

    private void showAlert(String message, DialogInterface.OnClickListener agreesListerner) {
        new AlertDialog.Builder((Activity) mContext)
                .setTitle("需要权限")
                .setMessage(message)
                .setPositiveButton("同意", agreesListerner)
                .setNegativeButton("拒绝", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(mContext, "已拒绝使用", Toast.LENGTH_SHORT).show();
                        mScanPermissionReCallback.onFaile();
                    }
                })
                .create().show();
    }

    public void checkPermissing(){
        if (!checkPermissions(permissions)) {
            ActivityCompat.requestPermissions((Activity) mContext, permissions, 0);
        }else {
            mScanPermissionReCallback.onSuccess();
        }
    }
}
