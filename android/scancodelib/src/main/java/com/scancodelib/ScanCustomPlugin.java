package com.scancodelib;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.honeywell.barcode.HSMDecodeResult;
import com.honeywell.plugins.PluginResultListener;
import com.honeywell.plugins.SwiftPlugin;

import java.util.List;

/*
 * This plug-in does nothing more than render text in the middle of the screen and silently return decode results to its listeners
 */
public class ScanCustomPlugin extends SwiftPlugin
{
	private TextView tvMessage;
	private int clickCount = 0;

	public ScanCustomPlugin(Context context)
	{
		super(context);
		//inflate the base UI layer
		View.inflate(context, R.layout.scan_custom_plugin_layout, this);
	}

	@Override
	public void onStart()
	{
		super.onStart();
	}

	@Override
	protected void onStop()
	{
		super.onStop();
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();
	}


	@Override
	public void onDecode(HSMDecodeResult[] results)
	{
		super.onDecode(results);

		notifyListeners(results);
	}

	@Override
	protected void onDecodeFailed()
	{
		super.onDecodeFailed();
	}

	@Override
	protected void onImage(byte[] image, int width, int height)
	{
		super.onImage(image, width, height);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh)
	{
		super.onSizeChanged(w, h, oldw, oldh);
	}

	private void notifyListeners(HSMDecodeResult[] results)
	{
		//tells all plug-in monitor event listeners we have a result (used by the system)
		this.finish();

		//notify all plug-in listeners we have a result
		List<PluginResultListener> listeners = this.getResultListeners();
		for(PluginResultListener listener : listeners)
			((ScanCustomPluginResultListener)listener).onCustomPluginResult(results);
	}
}
