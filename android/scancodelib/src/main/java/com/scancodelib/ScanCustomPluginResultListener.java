package com.scancodelib;

import com.honeywell.barcode.HSMDecodeResult;
import com.honeywell.plugins.PluginResultListener;

//this interface defines how the custom plug-in will return results to its listeners (must extend PluginResultListener)
public interface ScanCustomPluginResultListener extends PluginResultListener
{
	public void onCustomPluginResult(HSMDecodeResult[] barcodeData);
}