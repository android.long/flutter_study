/*
 * Copyright (C) 2018 xuexiangjys(xuexiangjys@163.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.scancodelib;


/**
 * <pre>
 *     desc   : 二维码 XQRCode API
 *     author : xuexiang
 *     time   : 2018/5/4 上午12:33
 * </pre>
 */
public class ScanCode {
    /**
     * ScanCaptureFragment
     */
    public static final String KEY_LAYOUT_ID = "key_layout_id";
    /**
     * 设置是否打开调试
     *
     * @param isDebug
     */
    public static void debug(boolean isDebug) {
        ScanLogUtils.setmIsDubug(isDebug);
    }

    /**
     * 设置调试模式
     *
     * @param tag
     */
    public static void debug(String tag) {
        ScanLogUtils.d(tag);
    }

    //==================================ScanCaptureFragment=================================//

    /**
     * 获取CaptureFragment设置layout参
     *
     * @param layoutId
     */
    public static ScanCaptureFragment getCaptureFragment(int layoutId) {
        return ScanCaptureFragment.newInstance(layoutId);
    }


    //================================FlashLight===================================//


}
