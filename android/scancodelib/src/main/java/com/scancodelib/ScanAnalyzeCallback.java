package com.scancodelib;

import android.graphics.Bitmap;

/**
 * Create by alv1 on 2019/6/12
 */

/**
 * 解析二维码结果
 */
public interface ScanAnalyzeCallback {

    /**
     * 解析成功
     *
     * @param bitmap 二维码图片
     * @param result 解析结果
     */
    void onAnalyzeSuccess(Bitmap bitmap, String result);

    /**
     * 解析失败
     */
    void onAnalyzeFailed();
}

