package io.flutter.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Create by alv1 on 2019/6/19
 */
public class ToastUtils {
    static Toast sMToast;
    public static void showToast(String msg, Context pContext){
        if (msg==null||pContext==null){
            return;
        }
        if (sMToast==null) {
            sMToast=Toast.makeText(pContext,msg,Toast.LENGTH_SHORT);
        }else {
            sMToast.setText(msg);
        }
        sMToast.show();
    }
}
