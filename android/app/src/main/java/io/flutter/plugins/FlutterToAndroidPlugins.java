package io.flutter.plugins;

import io.flutter.PluginConfig;
import io.flutter.app.FlutterActivity;
import io.flutter.call.DealMethodCall;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;

/**
 * flutter 调用 android 原生方法
 */
public class FlutterToAndroidPlugins implements MethodChannel.MethodCallHandler {

    private FlutterActivity activity;

    private FlutterToAndroidPlugins(FlutterActivity activity) {
        this.activity = activity;
    }

    public static void registerWith(FlutterActivity activity) {
        FlutterToAndroidPlugins instance = new FlutterToAndroidPlugins(activity);
        //flutter调用原生
        MethodChannel channel = new MethodChannel(activity.registrarFor(PluginConfig.channels_flutter_to_native_test)
                .messenger(), PluginConfig.channels_flutter_to_native_test);
        //setMethodCallHandler在此通道上接收方法调用的回调
        channel.setMethodCallHandler(instance);
    }

    @Override
    public void onMethodCall(MethodCall methodCall, MethodChannel.Result result) {
        DealMethodCall.onMethodCall(activity, methodCall, result);
    }

}