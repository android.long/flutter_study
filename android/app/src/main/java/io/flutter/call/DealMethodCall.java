package io.flutter.call;

import android.util.Log;

import java.util.HashMap;
import android.content.Intent;
import io.flutter.PluginConfig;
import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.utils.ToastUtils;
import com.base.cameralibrary.activity.CameraExampOpenActivity;
import com.study.flutter_study_test.ScanCodeActivity;

public class DealMethodCall {
    static String TAG =DealMethodCall.class.getSimpleName();

    /**
     * flutter调用原生方法的回调
     *
     * @param activity   activity
     * @param methodCall methodCall
     * @param result     result
     */
    public static void onMethodCall(FlutterActivity activity, MethodCall methodCall, final MethodChannel.Result result) {
        if (PluginConfig.methodTestNames.get("register").equals(methodCall.method)) {
            Log.e(TAG,"onMethodCall");
            //注册账号
            //此处处理业务（线程）
            result.success("succss");//处理后回调给Flutter
        }else  if (PluginConfig.methodTestNames.get("toast").equals(methodCall.method)) {
            Log.e(TAG,"toast");
            Object lMessage = methodCall.argument("message");
            ToastUtils.showToast((String) lMessage,activity);
        }
        else  if (PluginConfig.methodTestNames.get("camer").equals(methodCall.method)) {
            Log.e(TAG,"camer");
            Intent lIntent = new Intent(activity.getApplicationContext(), CameraExampOpenActivity.class);
            lIntent.putExtra("cropWidth",500);
            lIntent.putExtra("cropHeight",200);
            lIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.getApplicationContext().startActivity(lIntent);

        }
        else  if (PluginConfig.methodTestNames.get("scan").equals(methodCall.method)) {
            Log.e(TAG,"scan");
            Intent lIntent = new Intent(activity.getApplicationContext(), ScanCodeActivity.class);
            lIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activity.getApplicationContext().startActivity(lIntent);

        }
    }



    /**原生调用flutter方法的回调
     * @param activity activity
     * @param o o
     * @param eventSink eventSink
     */
    public static void onListen(FlutterActivity activity, Object o, EventChannel.EventSink eventSink){
        //在此调用
        eventSink.success("onConnected");

    }

    /**原生调用flutter方法的回调
     * @param activity activity
     * @param o o
     */
    public static void onCancel(FlutterActivity activity, Object o) {

    }
}