package com.study.flutter_study_test;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.scancodelib.ScanAnalyzeCallback;
import com.scancodelib.ScanCaptureFragment;
import com.scancodelib.ScanCode;
import com.scancodelib.ScanLogUtils;
import com.scancodelib.ScanPermissionReCallback;
import com.scancodelib.ScanPermissionUtils;


public class ScanCodeActivity extends Activity {


    private ScanPermissionUtils mScanPermissionUtils;
    private ScanCaptureFragment mCaptureFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scaner_layout);

        setFragment();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /**
     * 初始化控件
     */
    protected void setFragment() {

        mScanPermissionUtils = new ScanPermissionUtils(this, new ScanPermissionReCallback() {
            @Override
            public void onSuccess() {
                // 为二维码扫描界面设置定制化界面
                mCaptureFragment = ScanCode.getCaptureFragment(R.layout.activity_scaner_code);
                mCaptureFragment.setScanAnalyzeCallback(mScanAnalyzeCallback);
                getFragmentManager().beginTransaction().replace(R.id.fl_my_container, mCaptureFragment).commit();
            }

            @Override
            public void onFaile() {
                finish();
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mScanPermissionUtils.onRequestPermissionsResult(permissions);
    }

    /**
     * 二维码解析回调函数
     */
    ScanAnalyzeCallback mScanAnalyzeCallback = new ScanAnalyzeCallback() {
        @Override
        public void onAnalyzeSuccess(Bitmap bitmap, String result) {


            ScanLogUtils.d("二维码解析回调函数 " + result);
            //扫描多次
//            Message messageRestart = Message.obtain(mCaptureFragment.getHandler(), 12, null);
//            messageRestart.sendToTarget();

        }

        @Override
        public void onAnalyzeFailed() {

        }
    };


}
