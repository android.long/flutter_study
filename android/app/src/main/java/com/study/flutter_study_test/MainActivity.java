package com.study.flutter_study_test;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import io.flutter.app.FlutterActivity;
import io.flutter.plugins.AndroidToFlutterPlugins;
import io.flutter.plugins.FlutterToAndroidPlugins;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
  
  private ImageRecive mImageRecive;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);
    FlutterToAndroidPlugins.registerWith(this);
    AndroidToFlutterPlugins.registerWith(this);
    //注册Toast插件
    //ToastProviderPlugin.register(this, flutterView)
    
    mImageRecive = new ImageRecive();
    IntentFilter lIntentFilter = new IntentFilter();
    lIntentFilter.addAction("cameraactivityfinish");
    registerReceiver(mImageRecive,lIntentFilter);
  }
  
  @Override
  protected void onDestroy() {
    super.onDestroy();
    unregisterReceiver(mImageRecive);
  }
  private Handler mHandler=new Handler(Looper.myLooper()){
    @Override
    public void handleMessage(Message msg) {
      super.handleMessage(msg);
      if (msg.what==100) {
        String imagePath = (String) msg.obj;
        AndroidToFlutterPlugins.imageSelectCallBack(imagePath);
      }else {
        AndroidToFlutterPlugins.imageSelectCallBack(null);
      }
    }
  };
  private class ImageRecive extends BroadcastReceiver{
  
    @Override
    public void onReceive(Context context, Intent intent) {
      if (intent != null) {
        int lCode = intent.getIntExtra("code", 0);
        if (lCode==100){
          String filePath = intent.getStringExtra("filePath");
          Message lMessage = Message.obtain();
          lMessage.obj = filePath;
          lMessage.what=100;
          mHandler.sendMessage(lMessage);
        }else {
          mHandler.sendEmptyMessage(0);
        }
      }
    }
  }
}
